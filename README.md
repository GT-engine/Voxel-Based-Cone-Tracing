This project contains code for rendering scenes using voxel based code
tracing using both sparse voxel octree and 3D-clip-maps.

It was created as part of a bachelors project at KTH and its
corresponding report can be found here http://www.diva-portal.org/smash/record.jsf?pid=diva2%3A1216324&dswid=-9487

If there would be any interest in clarification of the code please
contact the authors on tmore@kth.se or gussch@kth.se.


In this repo all datafiles for the scene is missing, the sponza scene
can be found on the crytek website.

Dependecies:
- DevIL
- glfw
- gl
- glm
