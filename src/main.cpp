#include <iostream>
#include "view/window.hpp"

#include "scene/scene.hpp"
#include "scene/scene_object.hpp"

#include "renderer/renderer.hpp"
#include "renderer/simple_renderer.hpp"
#include "renderer/octree_renderer.hpp"
#include "renderer/clipmap_renderer.hpp"

#include "util/gl/shapes.hpp"
#include "util/gl/color_texture.hpp"

void add_test_cubes(Scene& scene){
  ShapeData* cube_data = Shape::cube();
  GLData     gl_data = Shape::to_gl_data(cube_data, 0, 1, 2, 3, 4);
  Texture2D* red = make_texture2d(Texture::BLUE);

  SceneObject* cube = new SceneObject(gl_data.vao,
					  cube_data->indices.size());
  cube->set_texture(SceneObject::DIFFUSE_TEXTURE, red);
  cube->size(50);
  cube->move_to(-100, 100, 0);
  cube->update_model_mat();
  cube->setEmission(glm::vec3(0, 0, 1.));

  static float f = 0;
  cube->add_behaviour([](SceneObject* o){
      //o->rotate_x(3.1415 / 32);
      //o->rotate_y(3.1415 / 32);
      o->translate_x(sin(f) * 5);
      f += 0.1;
      return true;

    });

  scene.add_dyamic_object(cube);
}


int main(void) {
  Scene scene("./data/crytec-sponza/sponza.obj");
  //SimpleRenderer renderer;

  //OctreeRenderer renderer(1280, 720);
  ClipmapRenderer renderer(1280, 720);
  Window window("Test", 1280, 720, renderer, scene);

  add_test_cubes(scene);
  if(!window.all_ok()){
    std::cerr << "Window failed to initialize: " << std::endl
	     << window.get_error_msg() << std::endl;
    exit(1);
  }
  window.run();
  //window.run_performance_checks();

  std::cout << "Loopie loop done" << std::endl;
}
