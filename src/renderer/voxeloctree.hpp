#pragma once


#include <vector>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include "scene/scene.hpp"
#include "scene/world_data.hpp"
#include "util/gl/texture3d.hpp"


class VoxelOctree {
private:




  Texture3D voxel_bricks_;
  Texture3D octree_node_;


  AtomicCounter voxel_brick_counter_;
  AtomicCounter octree_counter_;


  void make_voxel_list(Scene& scene);

public:

  VoxelOctree(){};

  VoxelOctree();


  void construct_octree(Scene& scene);

};
