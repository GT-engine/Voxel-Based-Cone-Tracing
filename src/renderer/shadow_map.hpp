#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <string>
#include <glm/glm.hpp>

#include "../scene/scene.hpp"
#include "../util/gl/program.hpp"
#include "quad_vao.hpp"

class Shadow_Map {
private:

  std::string error_msg_;

  Program shadow_program_;
  Program visualization_program_;

  Quad_VAO quad_vao_;

  GLuint depth_map_;
  GLuint depth_map_framebuffer_;

  int shadow_width_, shadow_height_;
  glm::vec3 light_look_at_, light_source_position_;
  glm::mat4 light_space_matrix_;

  void calculate_light_space_matrix();

public:
  Shadow_Map();

  Shadow_Map(int shadow_texture_width, int shadow_texture_height,
	     glm::vec3 light_look_at, glm::vec3 light_source_position);

  bool init();
  void render(Scene& scene);
  void visualize();

  void bind_texture(GLuint layout);

  std::string get_error_msg(){ return error_msg_;};
  glm::mat4 get_light_space_matrix(){ return light_space_matrix_;}

  int get_height(){return shadow_height_;}
  int get_width(){return shadow_width_;}

  glm::vec3 get_position(){return light_source_position_;};
  void set_position(glm::vec3 v);

  void bind_framebuffer();
};
