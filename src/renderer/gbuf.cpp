#include "gbuf.hpp"


GBuf::GBuf() : GBuf(600,600) {
}

GBuf::GBuf(int render_width, int render_height) {

  render_width_ = render_width;
  render_height_ = render_height;

}

bool GBuf::load_shaders(){
  gbuf_program_ = Program("shaders/vbct/g_buf.vert",
			  "shaders/vbct/g_buf.frag");
  if(!gbuf_program_.linked()){
    error_msg_ = "GBUFFER program failed: "
	       + gbuf_program_.get_error_msg();
    return false;
  }
  return true;
}

bool GBuf::load_render_targets(){
  int gl_error = GL_NO_ERROR;

  glGenFramebuffers(1, &g_buffer_);
  glBindFramebuffer(GL_FRAMEBUFFER, g_buffer_);

  glGenTextures(NUM_GBUF_TARGETS, textures);

  // Position buffer
  glBindTexture(GL_TEXTURE_2D, textures[POSITION]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, render_width_, render_height_, 0, GL_RGB, GL_FLOAT, NULL);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textures[POSITION], 0);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "CLIPMAP RENDERER: Failed to generate position texture. GL_ERROR " + std::to_string(gl_error);
    return false;
  }

  // - normal color buffer
  glBindTexture(GL_TEXTURE_2D, textures[NORMAL]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, render_width_, render_height_, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, textures[NORMAL], 0);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "GBUF: Failed to generate normal texture. GL_ERROR " + std::to_string(gl_error);
    return false;
  }

  // - color + specular color buffer
  glBindTexture(GL_TEXTURE_2D, textures[ALBEDO_SPEC]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, render_width_, render_height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, textures[ALBEDO_SPEC], 0);
  //glGenerateMipmap(GL_TEXTURE_2D);

  // Tangents
  glBindTexture(GL_TEXTURE_2D, textures[TANGENT]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, render_width_, render_height_, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, textures[TANGENT], 0);

  glBindTexture(GL_TEXTURE_2D, 0);

  glBindTexture(GL_TEXTURE_2D, textures[EMISSION]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, render_width_, render_height_, 0, GL_RGB,
               GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D,
                         textures[EMISSION], 0);
  glBindTexture(GL_TEXTURE_2D, 0);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "GBUF: Failed to generate albedo texture. GL_ERROR " + std::to_string(gl_error);
    return false;
  }

  // - tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
  unsigned int attachments[NUM_GBUF_TARGETS] = { GL_COLOR_ATTACHMENT0,
						 GL_COLOR_ATTACHMENT1,
						 GL_COLOR_ATTACHMENT2,
						 GL_COLOR_ATTACHMENT3,
                                                 GL_COLOR_ATTACHMENT4 };
  glDrawBuffers(NUM_GBUF_TARGETS, attachments);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "GBUF: Failed to bind draw buffers. GL_ERROR " + std::to_string(gl_error);
    return false;
  }

  glGenRenderbuffers(1, &render_buffer_);
  glBindRenderbuffer(GL_RENDERBUFFER, render_buffer_);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, render_width_, render_height_);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, render_buffer_);
  // finally check if framebuffer is complete
  GLuint framebuffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if(framebuffer_status != GL_FRAMEBUFFER_COMPLETE){
    error_msg_ = "GBUF: Framebuffer not complete. Framebuffer status "
	       + std::to_string(framebuffer_status);

    return false;
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  return true;
}

bool GBuf::init(){
  if(!quad_vao_.init()){
    error_msg_ = quad_vao_.get_error_msg();
    return false;
  }
  if(!load_shaders()){
    return false;
  }
  if(!load_render_targets()){
    return false;
  }
  return true;
}

void GBuf::blit(){
  glBindFramebuffer(GL_READ_FRAMEBUFFER, g_buffer_);
    {
    GLuint gl_error = glGetError();
    if(gl_error != GL_NO_ERROR){
      std::cerr << "AAAA" << gl_error << std::endl;
      exit(1);
    }
  }
   // write to default framebuffer
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  {
    GLuint gl_error = glGetError();
    if(gl_error != GL_NO_ERROR){
      std::cerr << "DDDD" << gl_error << std::endl;
      exit(1);
    }
  }
  glBlitFramebuffer(0, 0, render_width_, render_height_, 0, 0,
		    render_width_, render_height_, GL_DEPTH_BUFFER_BIT,
		    GL_NEAREST);
  {
    GLuint gl_error = glGetError();
    if(gl_error != GL_NO_ERROR){
      std::cerr << "CCCC" << gl_error << std::endl;
      exit(1);
    }
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void GBuf::render(Camera& camera, Scene& scene){
  glViewport(0,0, render_width_, render_height_);

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glBindFramebuffer(GL_FRAMEBUFFER, g_buffer_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  //glEnable(GL_BLEND);
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glm::mat4 perspective = camera.perspective_matrix();
  glm::mat4 look_at = camera.look_at_matrix();

  gbuf_program_.set_uniform_matrix4f("projection_mat", perspective);
  gbuf_program_.set_uniform_matrix4f("view_mat", look_at);

  gbuf_program_.set_uniform1i("diffuse", 0);
  gbuf_program_.set_uniform1i("normal_map", 1);
  gbuf_program_.set_uniform1i("specular", 2);


  gbuf_program_.use();

  gbuf_program_.set_uniform_matrix4f("model_mat", glm::mat4(1));
  for( auto object : scene.get_static_objects()){
    gbuf_program_.set_uniform3f("emission", object->getEmission());
    object->bind_vao();
    object->bind_texture(SceneObject::DIFFUSE_TEXTURE, 0);
    object->bind_texture(SceneObject::NORMALS_TEXTURE, 1);
    object->bind_texture(SceneObject::SPECULAR_TEXTURE, 2);
    object->draw();
  }

  for( auto object : scene.get_dynamic_objects()){
    gbuf_program_.set_uniform3f("emission", object->getEmission());
    object->bind_vao();
    gbuf_program_.set_uniform_matrix4f("model_mat", object->get_model_mat());
    object->bind_texture(SceneObject::DIFFUSE_TEXTURE, 0);
    object->bind_texture(SceneObject::NORMALS_TEXTURE, 1);
    object->bind_texture(SceneObject::SPECULAR_TEXTURE, 2);
    object->draw();
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GBuf::bind_texture(RenderTarget texture, GLuint location){
  glActiveTexture(GL_TEXTURE0 + location);
  glBindTexture(GL_TEXTURE_2D, textures[texture]);
}

void GBuf::unbind_textures(){
  glBindTexture(GL_TEXTURE_2D, 0);
}
