#pragma once

#include <vector>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "scene/scene.hpp"
#include "scene/world_data.hpp"
#include "view/camera.hpp"
#include "quad_vao.hpp"
#include "renderer.hpp"
#include "shadow_map.hpp"
#include "util/gl/texture3d.hpp"

class ClipmapVoxelizer {
public:
  ClipmapVoxelizer() {};
  ClipmapVoxelizer(int levels);
  ClipmapVoxelizer(int levels, float resolution, float dimensions);

  bool init();
  void voxelize(Scene& scene, Camera& camera, Shadow_Map& shadow_map);
  
  string get_error_msg() { return ""; }
  Texture3D& get_texture() { return voxel_texture_final_; }
  
private:
  static const int DEFAULT_FIELD_RESOLUTION = 128;
  static constexpr float DEFAULT_DIMENSIONS = 400.0f;
  
  Texture3D voxel_texture_;
  Texture3D voxel_texture_final_;
  
  Program voxel_program_;
  Program voxel_mip_program_;

  int levels_;
  int resolution_;
  int dimensions_;
};
