#pragma once


#include <GL/glew.h>
#include <GL/gl.h>
#include <string>

class Quad_VAO{
private:

  std::string error_msg_;
  bool initialized_ = false;
  GLuint quad_vao_;


public:

  Quad_VAO();

  bool init();

  void bind();
  void draw();

  std::string get_error_msg(){ return error_msg_;};

};
