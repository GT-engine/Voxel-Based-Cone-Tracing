#pragma once

#include <GL/gl.h>
#include "renderer.hpp"
#include "util/gl/program.hpp"
#include "quad_vao.hpp"
#include "clipmap_voxelizer.hpp"
#include "shadow_map.hpp"

#include "gbuf.hpp"

class ClipmapRenderer : public Renderer {
private:
  const int VOXEL_FIELD_SIZE = 512;

  Program voxelization_program_;
  Program gbuf_program_;
  Program lighting_program_;

  GLuint voxel_texture_ = 0;


  Quad_VAO quad_vao_;
  Shadow_Map shadow_map_;
  GBuf gbuf_;

  ClipmapVoxelizer voxelizer_;
  int voxel_resolution_;
  int clipmap_levels_;
  int voxel_base_dimension_;


  bool load_shaders();
  bool load_render_targets();

  void render_lighting(Camera& camera);

public:
  ClipmapRenderer();
  ClipmapRenderer(int render_width, int render_height);

  void voxelize();

  string get_error_msg() { return ""; }

  virtual bool init();
  virtual void render(Camera& camera, Scene& scene);
  virtual std::string performance_report(Camera& camera, Scene& scene, GLFWwindow* window){}
};
