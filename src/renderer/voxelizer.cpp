#include "voxelizer.hpp"



Voxelizer::Voxelizer(int voxel_field_size, glm::vec3 world_box){
  voxel_field_size_ = voxel_field_size;
  num_voxels_ = voxel_field_size_ * voxel_field_size_ * voxel_field_size_;
  world_box_ = world_box;
  voxel_projection_ = glm::ortho(-world_box_.x, world_box_.x,
				 -world_box_.y, world_box_.y,
				 world_box_.z, -world_box_.z);

}

bool Voxelizer::init(){
  if(!quad_vao_.init()){
    error_msg_ = "VOXELIZER: Quad failed to init " + quad_vao_.get_error_msg();
    return false;
  }

  GLuint gl_error = GL_NO_ERROR;

  voxel_program_ = Program("shaders/voxelizer/voxelization.vert",
			   "shaders/voxelizer/voxelization.geom",
			   "shaders/voxelizer/voxelization.frag");
  if(!voxel_program_.linked()){
    error_msg_ = "VOXELIZER: Voxelizer program failed to link: "
	       + voxel_program_.get_error_msg();
    return false;
  }

  // Cube render program
  trace_program_ = Program("shaders/voxelizer/voxel_trace.vert",
			  "shaders/voxelizer/voxel_trace.frag");
  if(!trace_program_.linked()){
    error_msg_ = "Voxel trace program failed: "
	       + trace_program_.get_error_msg();
    return false;
  }

  // Voxel texture
  voxel_texture_= Texture3D(voxel_field_size_, voxel_field_size_, voxel_field_size_, GL_RGBA8, 5);
  voxel_texture_.set_wrap(GL_CLAMP_TO_BORDER);
  voxel_texture_.set_min_mag(GL_LINEAR_MIPMAP_LINEAR,
			     GL_NEAREST);

  gl_error = glGetError();
  if(gl_error!= GL_NO_ERROR){
    error_msg_ = "VOXELIZER: Couldn't generate voxel texture. GL ERROR: "
	       + std::to_string(gl_error);
    return false;
  }

  glBindTexture(GL_TEXTURE_3D, 0);

  glBindVertexArray(0);

  return true;
}


void Voxelizer::render(Camera& camera, Scene& scene){
  visualize(camera);
}

void Voxelizer::voxelize(Scene& scene, Shadow_Map& sm){

  voxel_texture_.clear();

  voxel_program_.use();
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  glViewport(0,0,voxel_field_size_, voxel_field_size_);
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  voxel_program_.set_uniform_matrix4f("projection",
				      voxel_projection_);

  voxel_program_.set_uniform_matrix4f("light_projection",
				      sm.get_light_space_matrix());

  voxel_texture_.bind(0);

  voxel_program_.set_uniform1i("voxel_field",0);
  voxel_texture_.bind_image_texture(0);

  sm.bind_texture(GL_TEXTURE1);
  voxel_program_.set_uniform1i("shadow_map", 1);

  voxel_program_.set_uniform1i("voxel_size", voxel_field_size_);
    for( auto o : scene.get_static_objects()){
    o->bind_vao();
    o->bind_textures({0});
    o->draw();
  }

  voxel_texture_.gen_mipmaps();

  voxel_field_updated_ = false;

  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glEnable(GL_BLEND);
};




void Voxelizer::visualize(Camera& camera){
  trace_program_.use();
  glm::mat4 perspective = camera.perspective_matrix();
  glm::mat4 look_at = camera.look_at_matrix();

  trace_program_.set_uniform1f("fov_tan", 1.0);
  trace_program_.set_uniform1f("aspect_ratio", 16.0/9.0);

  trace_program_.set_uniform2f("resolution", glm::vec2(window_width_, window_height_));
  trace_program_.set_uniform3f("world_box", world_box_);

  voxel_texture_.bind(0);
  trace_program_.set_uniform1i("voxel_field", 0);

  trace_program_.set_uniform3f("camera_pos", camera.get_position_vec());
  trace_program_.set_uniform3f("camera_dir", camera.get_look_at_vec());
  trace_program_.set_uniform3f("camera_up", camera.get_up_vec());
  quad_vao_.draw();
}
