#pragma once

#include <string>

#include <GL/glew.h>
#include <GL/gl.h>


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iterator>

#include "../../scene/scene.hpp"
#include "../../scene/world_data.hpp"
#include "../../util/gl/shader_storage.hpp"
#include "../../util/gl/atomic_counter.hpp"
#include "../../util/gl/program.hpp"

class VoxelList {
  std::string error_msg_;

  GLuint buffer_size_;

  Program voxel_program_;
  ShaderStorage list_;
  AtomicCounter counter_;


public:
  VoxelList(){};
  VoxelList(GLuint buffer_size);

  bool init();

  void clear();

  void render(std::vector<SceneObject*> objects, glm::mat4 projection, GLuint voxel_field_size);

  void bind_list(GLuint list_location);
  void bind_counter(GLuint counter_location);

  GLuint get_num_voxels(){ return counter_.get();};
  std::string get_error_msg(){return error_msg_;}

  void set_count(GLuint n);
  GLuint get_count();

  void counter_checkpoint();
  void counter_reset();

};
