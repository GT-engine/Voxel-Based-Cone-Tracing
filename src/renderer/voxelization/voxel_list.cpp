#include "voxel_list.hpp"


VoxelList::VoxelList(GLuint buffer_size){
  buffer_size_ = buffer_size;

}

bool VoxelList::init(){
  GLuint gl_error = GL_NO_ERROR;

  voxel_program_ = Program("shaders/voxelizer/octree/voxelization.vert",
			   "shaders/voxelizer/octree/voxelization.geom",
			   "shaders/voxelizer/octree/voxelization.frag");
  if(!voxel_program_.linked()){
    error_msg_ = "Voxel List: voxel program didn't link " + voxel_program_.get_error_msg();
    return false;
  }
  counter_ = AtomicCounter(0);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "Voxel List: GL ERROR when creating atomic counter" + std::to_string(gl_error);
    return false;
  }

  list_ = ShaderStorage(buffer_size_);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "Voxel List: GL ERROR when creating texture1D" + std::to_string(gl_error);
    return false;
  }

  return true;
}


void VoxelList::clear(){
  counter_.set(0);
}

void VoxelList::render(std::vector<SceneObject*> objects, glm::mat4 projection, GLuint voxel_field_size){

  voxel_program_.use();

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0,0,voxel_field_size, voxel_field_size);

  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glEnable(GL_MULTISAMPLE);

  list_.bind(0);
  counter_.bind(1);

  voxel_program_.set_uniform_matrix4f("projection", projection);
  voxel_program_.set_uniform1i("voxel_count", 1);
  voxel_program_.set_uniform1ui("voxel_size", 512);
  voxel_program_.set_uniform1i("voxel_field",0);

  voxel_program_.set_uniform1i("diffuse", 0);

  for(auto o : objects){
    o->bind_vao();
    voxel_program_.set_uniform_matrix4f("model", o->get_model_mat());
    o->bind_texture(SceneObject::DIFFUSE_TEXTURE, 0);
    o->draw();
  }

  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  list_.unbind_buffer();
  counter_.unbind();
  glDisable(GL_MULTISAMPLE);
  // glEnable(GL_DEPTH_TEST);
  // glEnable(GL_CULL_FACE);
  // glEnable(GL_BLEND);
}



void VoxelList::bind_list(GLuint location){
  list_.bind(location);
}
void VoxelList::bind_counter(GLuint location){
  counter_.bind(location);
}


void VoxelList::counter_checkpoint(){
  counter_.checkpoint();
}

void VoxelList::counter_reset(){
  counter_.reset();
}
