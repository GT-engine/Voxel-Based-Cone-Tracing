#pragma once

#include <vector>
#include <string>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include "scene/scene.hpp"
#include "scene/world_data.hpp"
#include "view/camera.hpp"
#include "quad_vao.hpp"
#include "renderer.hpp"
#include "shadow_map.hpp"
#include "util/gl/texture3d.hpp"

#define DEFAULT_VOXEL_FIELD_SIZE 512
#define DEFAULT_WORLD_BOX  glm::vec3(2048,2048,2048);

class Voxelizer : public Renderer {

private:
  int voxel_field_size_;
  int num_voxels_;
  bool voxel_field_updated_ = false;

  Program voxel_program_;
  //GLuint voxel_texture_;
  Texture3D voxel_texture_;

  Program trace_program_;

  Quad_VAO quad_vao_;

  glm::vec3 world_box_;
  glm::mat4 voxel_projection_;

public:
  Voxelizer(){};
  Voxelizer(int voxel_field_size, glm::vec3 world_box);

  void voxelize(Scene& scene, Shadow_Map& sm);

  virtual bool init();
  virtual void render(Camera& camera, Scene& scene);

  //GLuint get_gl_voxel_texture(){ return voxel_texture_;};
  glm::vec3 get_world_box(){ return world_box_;};
  glm::mat4 get_voxel_projection(){ return voxel_projection_;};
  void visualize(Camera& camera);

  // Delete should never be called on this;
  Texture3D* get_texture(){return &voxel_texture_;};

  virtual std::string performance_report(Camera& camera, Scene& scene, GLFWwindow* window){}

};
