#include <GL/glew.h>
#include <GL/gl.h>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <cstdlib>


#include "octree_renderer.hpp"

OctreeRenderer::OctreeRenderer() : OctreeRenderer(600,600){

}
OctreeRenderer::OctreeRenderer(int render_width, int render_height) : Renderer(){

  render_width_ = render_width;
  render_height_ = render_height;

  // Note to sellf (bug) if sizes are > 1600 the injection of shadows doesn't allways work.
  shadow_map_ = Shadow_Map(1600,1600,
			   glm::vec3(0,0,0),
			   glm::vec3(0,1500,250));

  // Octree with nine levels (2 ^ 9) == 512
  octree_ = Octree(9);

  // Inital tests need 16777216 bytes of data for static scene
  voxel_list_ = VoxelList(2 << 26);
}

bool OctreeRenderer::load_shaders(){


  gbuf_program_ = Program("shaders/voxelizer/octree/vbct/g_buf.vert",
			  "shaders/voxelizer/octree/vbct/g_buf.frag");
  if(!gbuf_program_.linked()){
    error_msg_ = "GBUFFER program failed: "
	       + gbuf_program_.get_error_msg();
    return false;
  }

  finalize_program_ = Program("shaders/voxelizer/octree/vbct/finalize.vert",
			      "shaders/voxelizer/octree/vbct/finalize.frag");
  if(!finalize_program_.linked()){
    error_msg_ = "Finalize program failed: "
	       + finalize_program_.get_error_msg();
    return false;
  }

  // light_inject_program_ = Program("shaders/voxelizer/octree/light_inject.vert",
  //				  "shaders/voxelizer/octree/light_inject.frag"
  //				  );
  // if(!light_inject_program_.linked()){
  //   error_msg_ = "OCTREE Renderer: \n" +light_inject_program_.get_error_msg();
  //   return false;
  // }

  if(!voxel_list_.init()){
    error_msg_ = "VBCT Renderer: \n" + voxel_list_.get_error_msg();
    return false;
  }
  if(!octree_.init()){
    error_msg_ = "VBCT Renderer:\n" + octree_.get_error_msg();
    return false;
  }

  return true;
}


bool OctreeRenderer::load_render_targets(){
  int gl_error = GL_NO_ERROR;

  glGenFramebuffers(1, &g_buffer_);
  glBindFramebuffer(GL_FRAMEBUFFER, g_buffer_);

  // - position color buffer
  glGenTextures(1, &g_position_);
  std::cerr <<"first "<< g_position_ << std::endl;
  glBindTexture(GL_TEXTURE_2D, g_position_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, render_width_, render_height_, 0, GL_RGB, GL_FLOAT, NULL);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_position_, 0);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "VBCT RENDERER: Failed to generate position texture. GL_ERROR " + std::to_string(gl_error);
    return false;
  }

  // - normal color buffer
  glGenTextures(1, &g_normal_);
  glBindTexture(GL_TEXTURE_2D, g_normal_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, render_width_, render_height_, 0, GL_RGB, GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, g_normal_, 0);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "VBCT RENDERER: Failed to generate normal texture. GL_ERROR " + std::to_string(gl_error);
    return false;
  }

  // - color + specular color buffer
  glGenTextures(1, &g_albedo_spec_);
  glBindTexture(GL_TEXTURE_2D, g_albedo_spec_);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, render_width_, render_height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, g_albedo_spec_, 0);


  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "VBCT RENDERER: Failed to generate albedo texture. GL_ERROR " + std::to_string(gl_error);
    return false;
  }

  // - tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
  unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
  glDrawBuffers(3, attachments);

  gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    error_msg_ = "VBCT RENDERER: Failed to bind draw buffers. GL_ERROR " + std::to_string(gl_error);
    return false;
  }

  glGenRenderbuffers(1, &render_buffer_);
  glBindRenderbuffer(GL_RENDERBUFFER, render_buffer_);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, render_width_, render_height_);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, render_buffer_);
  // finally check if framebuffer is complete
  GLuint framebuffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if(framebuffer_status != GL_FRAMEBUFFER_COMPLETE){
    error_msg_ = "VBCT RENDERER: Framebuffer not complete. Framebuffer status "
	       + std::to_string(framebuffer_status);

    return false;
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  return true;
}


bool OctreeRenderer::init(){

  if(!quad_vao_.init()){
    error_msg_ = quad_vao_.get_error_msg();
    return false;
  }

  if(!shadow_map_.init()){
    error_msg_ = shadow_map_.get_error_msg();
    return false;
  }

  if(!load_shaders()){
    return false;
  }
  if(!load_render_targets()){
    return false;
  }
  return true;
}

void OctreeRenderer::render(Camera& camera, Scene& scene){

  if(scene.has_changes()){
    shadow_map_.render(scene);
    std::cout << "Making voxel list" << std::endl;
    voxel_list_.clear();

    voxel_list_.render(scene.get_static_objects(),
		       voxel_projection_,
		       512);


    std::cout << "Num static voxels: " << voxel_list_.get_num_voxels() <<std::endl;
    std::cout << "Making static octree" << std::endl;
    octree_.construct_static(voxel_list_);

    std::cout << "Construction done" << std::endl;
    scene.set_has_changes(false);
  }

  // static bool i = true;
  // if(i){
  //   voxel_list_.clear();
  //   voxel_list_.render(scene.get_dynamic_objects(),
  //		       voxel_projection_,
  //		       512);
  //   std::cout << "Num dynamic voxels: " << voxel_list_.get_num_voxels() << std::endl;
  //   octree_.construct_dynamic(voxel_list_);

  //   shadow_map_.render(scene);
  //   inject_light();

  //   // Note to self (Perofrmance shoud be able to set to true but this disables light mipmapping for some god damn reason);
  //   octree_.gen_mipmaps(false);
  // }
  // i = !i;

  // glBindFramebuffer(GL_FRAMEBUFFER, 0);
  // glViewport(0,0,window_width_,window_height_);
  // octree_.visualize(camera, window_width_, window_height_, voxel_projection_);
  // return;

  render_gbuf(camera, scene);
  glBindFramebuffer(GL_READ_FRAMEBUFFER, g_buffer_);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0); // write to default framebuffer

  glBlitFramebuffer(0, 0, render_width_, render_height_, 0, 0, render_width_, render_height_, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);


  render_finalize(camera);
  // write to default framebuffer
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

}


void OctreeRenderer::render_gbuf(Camera& camera, Scene& scene){
  glViewport(0,0, render_width_, render_height_);

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glBindFramebuffer(GL_FRAMEBUFFER, g_buffer_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  //glEnable(GL_BLEND);
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glm::mat4 perspective = camera.perspective_matrix();
  glm::mat4 look_at = camera.look_at_matrix();

  gbuf_program_.use();
  gbuf_program_.set_uniform_matrix4f("projection_mat", perspective);
  gbuf_program_.set_uniform_matrix4f("view_mat", look_at);

  gbuf_program_.set_uniform1i("diffuse", 0);
  gbuf_program_.set_uniform1i("normal_map", 1);
  gbuf_program_.set_uniform1i("specular_map", 2);

  gbuf_program_.set_uniform_matrix4f("model_mat", glm::mat4(1));
  for( auto o : scene.get_static_objects()){
    o->bind_vao();
    o->bind_texture(SceneObject::DIFFUSE_TEXTURE, 0);
    o->bind_texture(SceneObject::NORMALS_TEXTURE, 1);
    o->bind_texture(SceneObject::SPECULAR_TEXTURE, 2);
    o->draw();
  }
  for( auto o : scene.get_dynamic_objects()){
    o->bind_vao();
    gbuf_program_.set_uniform_matrix4f("model_mat", o->get_model_mat());
    o->bind_texture(SceneObject::DIFFUSE_TEXTURE, 0);
    o->bind_texture(SceneObject::NORMALS_TEXTURE, 1);
    o->bind_texture(SceneObject::SPECULAR_TEXTURE, 2);
    o->draw();
  }

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void OctreeRenderer::render_finalize(Camera& camera){

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0,0, window_width_,window_height_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  finalize_program_.use();

  finalize_program_.set_uniform1i("voxel_size", 512);

  finalize_program_.set_uniform_matrix4f("voxel_projection",
					 voxel_projection_);
  finalize_program_.set_uniform3f("camera_position",
				  camera.get_position_vec());
  finalize_program_.set_uniform_matrix4f("light_projection",
					 shadow_map_.get_light_space_matrix());

  finalize_program_.set_uniform3i("voxel_bricks_dimentions",
				  octree_.get_voxel_brick_dims());

  octree_.bind_octree_nodes(0);

  octree_.bind_voxel_bricks(GL_TEXTURE0);
  finalize_program_.set_uniform1i("voxel_bricks", 0);

    GLuint gl_error = glGetError();
  if(gl_error != GL_NO_ERROR){
    std::cerr << "GL ERROR WHILE RENDERING FINAL IMAGE " << gl_error << std::endl;
    exit(1);
  }

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, g_position_);
  finalize_program_.set_uniform1i("tex_position", 1);

  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, g_normal_);
  finalize_program_.set_uniform1i("tex_normal", 2);

  glActiveTexture(GL_TEXTURE3);
  glBindTexture(GL_TEXTURE_2D, g_albedo_spec_);
  finalize_program_.set_uniform1i("tex_albedo_spec", 3);

  shadow_map_.bind_texture(GL_TEXTURE4);
  finalize_program_.set_uniform1i("tex_shadow_map", 4);



  quad_vao_.draw();

}


void OctreeRenderer::inject_light(){

  light_inject_program_.use();
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0, 0, shadow_map_.get_width(), shadow_map_.get_height());
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);


  shadow_map_.bind_texture(GL_TEXTURE0);
  light_inject_program_.set_uniform1i("depth_map", 0);

  light_inject_program_.set_uniform1i("voxel_size", 512);
  light_inject_program_.set_uniform_matrix4f("light_proj_inverse",
					     glm::affineInverse(shadow_map_.get_light_space_matrix()));
  light_inject_program_.set_uniform_matrix4f("voxel_projection",
					     voxel_projection_);
  light_inject_program_.set_uniform3i("voxel_bricks_dimentions",
				      octree_.get_voxel_brick_buffer_size());
  octree_.bind_octree_nodes(0);
  octree_.bind_voxel_bricks(1);

  light_inject_program_.use();
  quad_vao_.bind();
  quad_vao_.draw();

  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

}
