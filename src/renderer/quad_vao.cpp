#include "quad_vao.hpp"
#include <iostream>


GLfloat quad_data_[] = {
  -1.0, 1.0, 0.0,
  1.0, -1.0, 0.0,
  1.0, 1.0, 0.0,

  -1.0, 1.0, 0.0,
  -1.0, -1.0, 0.0,
  1.0, -1.0, 0.0
};

Quad_VAO::Quad_VAO(){

}

bool Quad_VAO::init(){
  if(initialized_)
    return true;
  glGenVertexArrays(1, &quad_vao_);
  glBindVertexArray(quad_vao_);
  GLuint quad_vbo;
  glGenBuffers(1, &quad_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, quad_vbo);

  glBufferData(GL_ARRAY_BUFFER, sizeof(quad_data_), quad_data_, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0);

  int error = glGetError();
  if(error != GL_NO_ERROR){
    error_msg_ = "Quad VAO failed to initialize with gl error: " + std::to_string(error);
    return false;
  }
  initialized_ = true;
  return true;
}


void Quad_VAO::bind(){
  glBindVertexArray(quad_vao_);
}

void Quad_VAO::draw(){
  bind();
  glDrawArrays(GL_TRIANGLES, 0, 6);
}
