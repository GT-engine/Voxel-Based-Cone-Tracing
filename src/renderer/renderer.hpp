#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "../scene/scene.hpp"
#include "../view/camera.hpp"

class Renderer{
protected:
  const Camera* camera_;
  int window_width_;
  int window_height_;
  float window_dimentions_;

  std::string error_msg_;
  bool error = false;

  bool use_AO_ = true;

  void update_window_dimentions();

public:
  Renderer();

  void set_window_width(int width);
  void set_window_height(int height);
  virtual void set_window_dimentions(int width, int height);

  void set_camera(const Camera* cam);

  virtual bool init() = 0;
  virtual void render(Camera& camera, Scene& scene) = 0;
  virtual std::string performance_report(Camera& camera, Scene& scene, GLFWwindow * window) = 0;

  void toggle_AO();

  std::string get_error_msg(){ return error_msg_;};
};
