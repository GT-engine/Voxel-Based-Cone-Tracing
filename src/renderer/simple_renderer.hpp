#pragma once
#include <GL/gl.h>
#include "renderer.hpp"
#include "util/gl/program.hpp"
#include "voxelizer.hpp"
#include "shadow_map.hpp"

class SimpleRenderer : public Renderer {
private:
  Program program_;
  Voxelizer voxelizer_;
  Shadow_Map* shadow_map_;

public:
  SimpleRenderer();
  virtual bool init();
  virtual void render(Camera& camera, Scene& scene);
  virtual void set_window_dimentions(int width, int height);
  virtual std::string performance_report(Camera& camera, Scene& scene, GLFWwindow* window){}
};
