#pragma once
#include <chrono>
#include <GL/gl.h>
#include "renderer.hpp"
#include "util/gl/program.hpp"
#include "quad_vao.hpp"
#include "voxelizer.hpp"
#include "shadow_map.hpp"
#include "voxelization/octree.hpp"
#include "voxelization/voxel_list.hpp"
#include "gbuf.hpp"

class OctreeRenderer : public Renderer{
private:
  const int VOXEL_FIELD_SIZE = 512;

  Program voxelization_program_;
  Program light_inject_program_;

  Program finalize_program_;

  int render_width_, render_height_;

  GLuint voxel_texture_ = 0;

  Quad_VAO quad_vao_;
  Shadow_Map shadow_map_;

  VoxelList voxel_list_;
  Octree octree_;

  GBuf gbuf_;

  glm::mat4 voxel_projection_ = glm::ortho(-2048.0, 2048.0,
					   -2048.0, 2048.0,
					   2048.0, -2048.0);
  bool load_shaders();
  void render_finalize(Camera& camera);


public:
  OctreeRenderer();
  OctreeRenderer(int render_width, int render_height);

  void voxelize();
  virtual bool init();
  virtual void render(Camera& camera, Scene& scene);
  virtual std::string performance_report(Camera& camera, Scene& scene, GLFWwindow * window);
};
