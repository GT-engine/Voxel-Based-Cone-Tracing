#include "camera_store.hpp"




CameraStore::CameraStore(std::string file_path){
  fp_ = file_path;
  cameras_.resize(10);
  load_cameras();
}


void CameraStore::load_cameras(){
  std::ifstream f;
  f.open(fp_);
  if(f.is_open()){
    for(int i = 0; i < 10; i++){
      glm::vec3 look_at;
      glm::vec3 pos;
      glm::vec3 up;
      f >> look_at.x;
      f >> look_at.y;
      f >> look_at.z;
      f >> pos.x;
      f >> pos.y;
      f >> pos.z;
      f >> up.x;
      f >> up.y;
      f >> up.z;
      cameras_[i].set_look_at_vec(look_at);
      cameras_[i].set_position_vec(pos);
      cameras_[i].set_up_vec(up);
    }
  }
  f.close();
}

void CameraStore::store_cameras(){
  std::ofstream f(fp_);
  for(auto c : cameras_){
    auto la = c.get_look_at_vec();
    auto pos = c.get_position_vec();
    auto up = c.get_up_vec();
    f << la.x << std::endl << la.y << std::endl << la.z << std::endl
      << pos.x << std::endl << pos.y << std::endl << pos.z << std::endl
      << up.x << std::endl << up.y << std::endl << up.z  << std::endl;
  }
  f.close();
}

void CameraStore::save(Camera& cam, int i){
  cameras_[i] = cam;
  store_cameras();
}
Camera CameraStore::load(int i){
  return cameras_[i];
}
