#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera{
private:
  glm::vec3 look_at_ = glm::vec3(0.0,0.0,-1.0);
  glm::vec3 position_ = glm::vec3(0.0,0.0,0.0);
  glm::vec3 up_      = glm::vec3(0.0,1.0,0.0);

  float fovy_angle_ = 60.0f;
  float far_plane_ = 5000.0f;
  float near_plane_ = 0.1f;
  float aspect_ratio_ = 16.0 / 9.0f;

public:
  Camera();
  Camera(float fovy_angle, float far_plane, float near_plane);
  Camera(glm::vec3 look_at, glm::vec3 position, glm::vec3 up);
  Camera(glm::vec3 look_at, glm::vec3 position, glm::vec3 up, float fovy_angle,
	 float far_plane, float near_plane);

  void horizontal_rotate(float angle);
  void vertical_rotate(float angle);

  void move_forward(float step);
  void move_sideways(float step);
  void move_up(float step);

  void set_look_at(float x, float y, float z);
  void set_look_at(glm::vec3 look_at);

  glm::mat4 perspective_matrix();
  glm::mat4 look_at_matrix();

  glm::vec3 get_look_at_vec(){ return look_at_;};
  glm::vec3 get_position_vec(){ return position_;};
  glm::vec3 get_up_vec(){ return up_;};

  void set_look_at_vec(glm::vec3 v){look_at_=v;};
  void set_position_vec(glm::vec3 v){position_=v;};
  void set_up_vec(glm::vec3 v){up_=v;};

};
