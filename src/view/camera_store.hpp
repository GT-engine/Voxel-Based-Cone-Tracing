#pragma once
#include <vector>
#include "camera.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <glm/glm.hpp>

class CameraStore{
  std::vector<Camera> cameras_;
  std::string fp_;
  void load_cameras();
  void store_cameras();

public:
  CameraStore(){ };
  CameraStore(std::string file_path);

  void save(Camera& cam, int index);
  Camera load(int index);

};
