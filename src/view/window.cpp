#include "window.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <iostream>


#define DEFAULT_GL_MAJOR_VERSION 4
#define DEFAULT_GL_MINOR_VERSION 6

std::map<void*, Window*> Window::active_windows_ = std::map<void*, Window*>();


Window::Window(std::string title,
	       int init_height,
	       int init_width,
	       Renderer& renderer,
	       Scene& scene
	       ){
  cam_store_ = CameraStore("./data/cameras.dat");

  renderer_ = &renderer;
  scene_    = &scene;
  height_   = init_height;
  width_    = init_width;
  camera_.move_up(10.);
  if(!init_glfw()){
    all_ok_ = false;
    error_msg_ = "GLFW failed to init";
    return;
  }

  renderer.set_window_dimentions(init_width, init_height);

  window_ = glfwCreateWindow(width_, height_,
			     title.c_str(), NULL, NULL);

  if(window_ == nullptr){
    all_ok_ = false;
    error_msg_ = "GLFW failed to create window\n";
    return;
  }
  active_windows_[window_] = this;
  glfwMakeContextCurrent(window_);

  double xpos, ypos;
  glfwGetCursorPos(window_, &xpos, &ypos);
  prev_mouse_x_ = xpos;
  prev_mouse_y_ = ypos;

  glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  glfwSetFramebufferSizeCallback(window_, global_frambuffer_size_callback);
  glfwSetKeyCallback(window_, global_key_press_callback);
  glfwSetCursorPosCallback(window_, global_mouse_move_callback);
  glfwSetMouseButtonCallback(window_, global_mouse_button_callback);

  // Could be wrong as this might need to be called for every context
  // chagne, however not sure.
  glewInit();
  closed_ = false;

  if(!renderer.init()){
    all_ok_ = false;
    error_msg_ = "Renrerer failed to initialize!" + renderer.get_error_msg();
    return;
  }

  //glEnable(GL_DEBUG_OUTPUT);
  //glDebugMessageCallback(oglDebugCallbackFun, NULL);

  if(!scene.init(tex_store_)){
    all_ok_ = false;
    error_msg_ = "Couldn't initialize scene";
  }
  glfwGetCursorPos(window_, &xpos, &ypos);
  prev_mouse_x_ = xpos;
  prev_mouse_y_ = ypos;
};

Window::~Window(){
  if(!closed_){
    close();
  }
  if(active_windows_.size() == 0){
    glfwTerminate();
  }
}

void Window::run(){

  glfwMakeContextCurrent(window_);

  glClearColor(0.0, 0.0, 0.0, 1.0);
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

  running_main_loop_ = true;

  glfwSetTime(0);
  prev_time_ = 0.0;

  while(!should_pause_ && !should_close_ && !glfwWindowShouldClose(window_)){

    scene_->update_objects();

    renderer_->render(camera_, *scene_);

    if (toggle_AO_) {
      renderer_->toggle_AO();
      toggle_AO_ = false;
    }
    update_camera(glfwGetTime() - prev_time_);
    prev_time_ = glfwGetTime();

    glfwSwapBuffers(window_);

    glfwPollEvents();

  }
  running_main_loop_ = false;
  if(should_close_ || glfwWindowShouldClose(window_)){
    close();
  }
}

void Window::run_performance_checks(){
  std::cout << renderer_->performance_report(camera_, *scene_, window_);
}


bool Window::all_ok(){
  return all_ok_;
}

std::string Window::get_error_msg(){
  return error_msg_;
}

void Window::close(){
  if(running_main_loop_){
    should_close_ = true;
  }
  if(!closed_){
    active_windows_.erase(window_);
    glfwDestroyWindow(window_);
    closed_ = true;
  }
}


void Window::should_pause(){
  should_pause_ = true;
}
bool Window::paused(){
  return running_main_loop_;
}


void Window::frambuffer_size_callback(int width, int height){
  GLFWwindow* other_window  = glfwGetCurrentContext();
  glfwMakeContextCurrent(window_);

  width_ = width;
  height_ = height;
  glClear(GL_COLOR_BUFFER_BIT);

  renderer_->set_window_dimentions(width, height);

  glfwMakeContextCurrent(other_window);
}

void Window::mouse_move_callback(double xpos, double ypos){
  double delta_x = prev_mouse_x_ - xpos;
  double delta_y = prev_mouse_y_ - ypos;
  prev_mouse_x_ = xpos;
  prev_mouse_y_ = ypos;

  camera_.vertical_rotate(delta_x / 200.0);
  camera_.horizontal_rotate(delta_y / 200.0);

}

void Window::mouse_button_callback(int button, int action, int mods){
  switch(button){
  case GLFW_MOUSE_BUTTON_LEFT:
    glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    break;
  }
}

void Window::key_press_callback(int key, int scancode, int action, int mods){
  if(GLFW_KEY_0 <= key && key <= GLFW_KEY_9){
    int cam_idx = key - GLFW_KEY_0;
    if(mods & GLFW_MOD_SHIFT){
      cam_store_.save(camera_, cam_idx);
    } else {
      camera_ = cam_store_.load(cam_idx);
    }
  }
  if(action == GLFW_PRESS){
    switch(key){
    case GLFW_KEY_ESCAPE:
      glfwSetInputMode(window_, GLFW_CURSOR,  GLFW_CURSOR_NORMAL);
      break;
    case GLFW_KEY_W:
      move_forward_ = true;
      break;
    case GLFW_KEY_S:
      move_backwards_ = true;
      break;
    case GLFW_KEY_A:
      move_left_ = true;
      break;
    case GLFW_KEY_D:
      move_right_ = true;
      break;
    case GLFW_KEY_LEFT_SHIFT:
    case GLFW_KEY_RIGHT_SHIFT:
      move_down_ = true;
      break;
    case GLFW_KEY_SPACE:
      move_up_ = true;
      break;
    case GLFW_KEY_O:
      toggle_AO_ = true;
      break;
    }
  } else if(action == GLFW_RELEASE){
    switch(key){
    case GLFW_KEY_W:
      move_forward_ = false;
      break;
    case GLFW_KEY_S:
      move_backwards_ = false;
      break;
    case GLFW_KEY_A:
      move_left_ = false;
      break;
    case GLFW_KEY_D:
      move_right_ = false;
      break;
    case GLFW_KEY_LEFT_SHIFT:
    case GLFW_KEY_RIGHT_SHIFT:
      move_down_ = false;
      break;
    case GLFW_KEY_SPACE:
      move_up_ = false;
      break;
    }
  }
}

// Handlers for glfw events

void Window::global_glfw_error_callback(int error, const char* descrtiption){
  std::cerr << "GLFW Raised an error:" << error << std::endl
	    << descrtiption << std::endl;
}

Window* Window::get_window_by_glwf_window(GLFWwindow* glfw_window){
    try {
      return active_windows_.at((void*)glfw_window);
    } catch(const std::out_of_range& oor){
      return nullptr;
    }
}

void Window::global_frambuffer_size_callback(GLFWwindow* glfw_window, int width, int height){
  Window* window = get_window_by_glwf_window(glfw_window);
  if(window == nullptr){
    std::cerr << "GLFW rezise disregarded as window wasn't bound" << std::endl;
    return;
  }
  window->frambuffer_size_callback(width,height);
}

void Window::global_mouse_move_callback(GLFWwindow* glfw_window, double xpos, double ypos){
  Window* window = get_window_by_glwf_window(glfw_window);
  if(window == nullptr){
    std::cerr << "GLFW mouse move event disregarded as window wasn't bound" << std::endl;
    return;
  }
  window->mouse_move_callback(xpos,ypos);
}

void Window::global_mouse_button_callback(GLFWwindow* glfw_window, int button, int action, int mods){
  Window* window = get_window_by_glwf_window(glfw_window);;
  if(window == nullptr){
    std::cerr << "GLFW mouse button event disregarded as window wasn't bound" << std::endl;
    return;
  }
  window->mouse_button_callback(button, action, mods);

}

void Window::global_key_press_callback(GLFWwindow* glfw_window, int key, int scancode, int action, int mods){
    Window* window = get_window_by_glwf_window(glfw_window);
    if(window == nullptr){
    std::cerr << "GLFW key press event disregarded as window wasn't bound" << std::endl;
    return;
  }
  window->key_press_callback(key,scancode,action, mods);
}

void Window::oglDebugCallbackFun(GLenum source, GLenum type, GLuint id, GLenum severity,
				 GLsizei length,const GLchar *message,
				 const void *userParam) {

  if (severity > 35000) {
    std::cerr << severity << ": " << std::string(message) << std::endl;
  }
}

bool Window::init_glfw(){
  if(active_windows_.size() == 0){
    if(!glfwInit()){
      return false;
    }

    glfwSetErrorCallback(global_glfw_error_callback);
    //glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, DEFAULT_GL_MAJOR_VERSION);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, DEFAULT_GL_MAJOR_VERSION);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 2);
    glfwWindowHint(GLFW_DEPTH_BITS, INT_MAX);

    return true;
  }
  return true;
}


void Window::update_camera(double delta_t){
  double movement = 100. * delta_t;
  if(move_forward_)
    camera_.move_forward(movement);
  if(move_backwards_)
    camera_.move_forward(-movement);
  if(move_left_)
    camera_.move_sideways(-movement);
  if(move_right_)
    camera_.move_sideways(movement);
  if(move_up_)
    camera_.move_up(movement);
  if(move_down_)
    camera_.move_up(-movement);
}
