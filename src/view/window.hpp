#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>
#include <map>

#include "../scene/scene.hpp"
#include "../renderer/renderer.hpp"
#include "../util/gl/texture_store.hpp"
#include "camera_store.hpp"

class Window{
private:
  static std::map<void*, Window*> active_windows_;


  static Window* get_window_by_glwf_window(GLFWwindow* glfw_window);
  static void global_frambuffer_size_callback(GLFWwindow* glfw_window, int width, int height);
  static void global_mouse_move_callback(GLFWwindow* glfw_window, double xpos, double ypos);
  static void global_mouse_button_callback(GLFWwindow* glfw_window, int button, int action, int mods);

  static void global_key_press_callback(GLFWwindow* glfw_window, int key, int scancode, int action, int mods);
  static void global_glfw_error_callback(int error, const char* descrtiption);
  static void oglDebugCallbackFun(GLenum source, GLenum type, GLuint id, GLenum severity,
				  GLsizei length,const GLchar *message,
				  const void *userParam);

  static bool init_glfw();

  void update_camera(double delta_t);



private:
  CameraStore cam_store_;

  GLFWwindow* window_;
  Renderer* renderer_;
  Scene* scene_;
  TextureStore tex_store_;

  std::string error_msg_;
  bool all_ok_ = true;
  bool closed_ = true;
  bool should_close_ = false;
  bool should_pause_ = false;
  bool running_main_loop_ = false;

  double prev_mouse_x_ = 0, prev_mouse_y_ = 0;

  double prev_time_ = 0;

  bool move_forward_ = false;
  bool move_backwards_ = false;
  bool move_left_ = false;
  bool move_right_ = false;
  bool move_up_ = false;
  bool move_down_ = false;
  bool toggle_AO_ = false;

  int height_, width_;
  void frambuffer_size_callback(int width, int height);
  void mouse_move_callback(double xpos, double ypos);
  void mouse_button_callback(int button, int action, int mods);
  void key_press_callback(int key, int scancode, int action, int mods);

  Camera camera_;
public:
  Window(std::string title, int init_height, int init_width,
	       Renderer& renderer,  Scene& scene);
  ~Window();

  void run();

  void run_performance_checks();

  bool all_ok();
  std::string get_error_msg();

  void close();

  void should_pause();
  bool paused();

};
