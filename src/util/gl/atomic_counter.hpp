#pragma once

#include <GL/glew.h>
#include <GL/gl.h>


class AtomicCounter{
private:
  GLuint buffer_ = 0;
  GLuint checkpoint_value_ = 0;
public:
  // Uninitialized counter
  AtomicCounter(){};
  AtomicCounter(GLuint inital_value);
  ~AtomicCounter();

  void bind(GLuint binding);
  static void unbind();

  GLuint clear();
  GLuint set(GLuint new_value);
  GLuint get();

  void checkpoint();
  void reset();

  GLuint get_checkpoint_value(){return checkpoint_value_ ;}

};
