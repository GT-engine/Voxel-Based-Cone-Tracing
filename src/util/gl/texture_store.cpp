#include "texture_store.hpp"
#include <utility>
#include <iostream>

TextureStore::TextureStore(){};


TextureStore::~TextureStore(){
  for(auto it = texture_map_.begin(); it != texture_map_.end(); ++it){
    delete (*it).second;
  }
}


bool TextureStore::add(std::string name, Texture2D* tex){
  std::cerr << "Added " << name << std::endl;
  auto ret = texture_map_.insert({name,tex});
  return ret.second;

}

bool TextureStore::remove(std::string name){
  auto it = texture_map_.find(name);
  if(it != texture_map_.end()){
    delete (*it).second;
    texture_map_.erase(it);
    return true;
  }
  return false;
}

Texture2D* TextureStore::get(std::string name){
  auto it = texture_map_.find(name);
  if(it == texture_map_.end())
    return nullptr;
  return (*it).second;
}
