#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>

#include "texture2d.hpp"


namespace Texture {

  enum BasicColor {
    BLACK,
    WHITE,
    RED,
    GREEN,
    BLUE,
    N_COLORS
  };

  glm::vec4 to_vec4(BasicColor);


  Texture2D* make_texture2d(glm::vec4 color);
  Texture2D* make_texture2d(BasicColor color);

};
