#pragma once
#include <map>
#include <string>

#include "texture2d.hpp"

class TextureStore {
private:

  std::map<std::string, Texture2D*> texture_map_;

public:

  TextureStore();
  ~TextureStore();

  bool add(std::string name, Texture2D* tex);
  bool remove(std::string name);
  Texture2D* get(std::string name);
};
