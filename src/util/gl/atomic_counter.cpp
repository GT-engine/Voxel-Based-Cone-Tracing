#include "atomic_counter.hpp"




AtomicCounter::AtomicCounter(GLuint initial_value){
  glGenBuffers(1, &buffer_);
  glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, buffer_);
  glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(GLuint), &initial_value, GL_DYNAMIC_DRAW);
  glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
}

AtomicCounter::~AtomicCounter(){
  //if(buffer_ != 0)
  //  glDeleteBuffers(1, &buffer_);
}

void AtomicCounter::bind(GLuint binding){
  glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, buffer_);
  glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, binding, buffer_);
}

void AtomicCounter::unbind(){
  glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
}

GLuint AtomicCounter::set(GLuint new_value){
  GLuint *counters;
  GLuint old_value;
  glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, buffer_);
  // again we map the buffer to userCounters, but this time for read-only access
  counters = (GLuint*)glMapBufferRange(GL_ATOMIC_COUNTER_BUFFER,
				       0,
				       sizeof(GLuint),
					GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT
				       );
  old_value = *counters;
  *counters = new_value;
  glFlushMappedBufferRange(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint));
  glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
  return old_value;
}
GLuint AtomicCounter::get(){
  GLuint *counters;
  GLuint value;
  glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, buffer_);
  // again we map the buffer to userCounters, but this time for read-only access
  counters = (GLuint*)glMapBufferRange(GL_ATOMIC_COUNTER_BUFFER,
				       0,
				       sizeof(GLuint),
				       GL_MAP_READ_BIT
				       );
  value = *counters;
  glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
  return value;
}

GLuint AtomicCounter::clear(){
  return set(0);
}


void AtomicCounter::checkpoint(){
  checkpoint_value_ = get();
}

void AtomicCounter::reset(){
  set(checkpoint_value_);
}
