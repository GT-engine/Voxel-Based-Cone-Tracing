#pragma once

#include <GL/glew.h>
#include <GL/gl.h>


class ShaderStorage {
private:

  GLuint ssbo_ = 0;

public:

  ShaderStorage(){};
  ShaderStorage(GLuint size);
  ShaderStorage(GLuint size, GLvoid* data);
  void bind(GLuint binding);

  //  void clear(GLuint )
  // Wrapper for glBufferSubData
  void buffer_sub_data(GLintptr offset, GLsizeiptr size, const GLvoid* data);

  void bind_buffer();
  void unbind_buffer();

  void clear_subdata(GLintptr offset, GLsizeiptr size);
};
