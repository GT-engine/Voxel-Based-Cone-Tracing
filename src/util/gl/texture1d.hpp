#pragma once
#include <GL/glew.h>
#include <GL/gl.h>


class Texture1D {
private:
  GLuint buffer_ = 0;
  GLuint internal_format_;
public:
  Texture1D(){};
  Texture1D(GLuint size, GLuint internal_format);
  ~Texture1D();

  void bind(GLuint layout);
  void unbind();

  void bind_image_read(GLuint location);
  void bind_image_read(GLuint location, GLuint format);

  void bind_image_write(GLuint location);
  void bind_image_write(GLuint location, GLuint format);
  void bind_image(GLuint location);
  void bind_image(GLuint location, GLuint format);

};
