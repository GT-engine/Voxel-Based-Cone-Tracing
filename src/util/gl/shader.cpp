#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include "shader.hpp"

using namespace std;

Shader::Shader(string shader_fp, GLuint shader_type){
  std::string shader_type_name;

  switch(shader_type){
  case GL_VERTEX_SHADER:
    shader_type_name = "VERTEX SHADER";
    break;
  case GL_FRAGMENT_SHADER:
    shader_type_name = "FRAGMENT SHADER";
    break;
  case GL_GEOMETRY_SHADER:
    shader_type_name = "GEOMETRY SHADER";
    break;
  case GL_COMPUTE_SHADER:
    shader_type_name = "COMPUTE SHADER";
    break;
  default:
    compiled_ = false;
    std::ostringstream message;
    message <<  "SHADER ERROR invalid shader types passed to constructor  ("
	    << shader_type << ")";
    error_msg_ = message.str();
    return;
  }

  const char* shader_src;

  ifstream shader_file(shader_fp.c_str());


  if(!shader_file.is_open()){
    compiled_ = false;
    error_msg_ = "Cound't open " + shader_type_name + " file: " + shader_fp;
    return;
  }

  stringstream shader_sstream;
  string shader_string;
  shader_sstream << shader_file.rdbuf();
  shader_string = shader_sstream.str();
  shader_src = shader_string.c_str();
  shader_file.close();

  shader_id_ = glCreateShader(shader_type);

  glShaderSource(shader_id_, 1, &shader_src, NULL);

  glCompileShader(shader_id_);

  GLint success;
  glGetShaderiv(shader_id_, GL_COMPILE_STATUS, &success);

  if( !success ) {

    char info_log[1024];
    compiled_ = false;

    glGetShaderInfoLog(shader_id_, 1024, NULL, info_log);
    error_msg_ = shader_type_name + info_log;
    return;
  }
}


bool Shader::compiled(){
  return compiled_;
}

Shader::~Shader(){
  if(compiled_)
    glDeleteShader(shader_id_);
}

GLint Shader::get_glid(){

  return shader_id_;
}

string Shader::get_error_msg(){
  return error_msg_;
}
