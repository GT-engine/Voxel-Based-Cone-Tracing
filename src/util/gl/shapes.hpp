#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>


struct ShapeData {
  std::vector<GLfloat> vertices;
  std::vector<GLfloat> normals;
  std::vector<GLfloat> uv;
  std::vector<GLfloat> tangents;
  std::vector<GLfloat> bitangents;
  std::vector<GLint> indices;
};

struct GLData {
  GLuint vao;
  GLuint vertex_buffer;
  GLuint normal_buffer;
  GLuint uv_buffer;
  GLuint tangent_buffer;
  GLuint bitangent_buffer;
  GLuint indices_buffer;
};

namespace Shape {
  ShapeData* cube();
  // ShapeData* cube(glm::vec3 center_position);

  // ShapeData* box(glm::vec3 center_position, glm::vec3 side_lengths);

  // ShapeData* quad();
  // ShapeData* quad(glm::vec3 center_position, glm::vec2 side_lengths);

  GLData to_gl_data(ShapeData* shape,
                    GLuint vertex_location,
                    GLuint normal_location,
                    GLuint uv_location,
                    GLuint tangent_location,
                    GLuint bitangent_location);
};
