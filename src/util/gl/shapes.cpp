#include "shapes.hpp"

#include <iostream>

const std::vector<GLfloat> cube_vertices = {
  -1.0, 1.0, 1.0,
   1.0, 1.0, 1.0,
   1.0, 1.0,-1.0,
  -1.0, 1.0,-1.0,
  -1.0,-1.0, 1.0,
   1.0,-1.0, 1.0,
   1.0,-1.0,-1.0,
  -1.0,-1.0,-1.0
};

const std::vector<GLint> cube_indices = {
  7, 3, 2,
  7, 2, 6,
  6, 2, 1,
  6, 1, 5,
  5, 1, 0,
  5, 0, 4,
  4, 0, 3,
  4, 3, 7,
  3, 0, 1,
  3, 1, 2,
  7, 5, 4,
  7, 6, 5
};

ShapeData* Shape::cube(){
  return new ShapeData{
    std::vector<GLfloat>(cube_vertices),
      std::vector<GLfloat>(),
      std::vector<GLfloat>(),
      std::vector<GLfloat>(),
      std::vector<GLfloat>(),      
      std::vector<GLint>(cube_indices)
  };
}

GLData Shape::to_gl_data(ShapeData* shape,
			 GLuint vertex_location,
			 GLuint normal_location,
			 GLuint uv_location,
                         GLuint tangent_location,
                         GLuint bitangent_location){
  GLData result = {0,0,0,0,0};

  glGenVertexArrays(1, &result.vao);
  glGenBuffers(1, &result.vertex_buffer);
  glGenBuffers(1, &result.normal_buffer);
  glGenBuffers(1, &result.uv_buffer);
  glGenBuffers(1, &result.indices_buffer);
  glGenBuffers(1, &result.tangent_buffer);
  glGenBuffers(1, &result.bitangent_buffer);

  glBindVertexArray(result.vao);

  if(!shape->vertices.empty()){

    glBindBuffer(GL_ARRAY_BUFFER, result.vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER,
		 sizeof(float) * shape->vertices.size(),
		 shape->vertices.data(),
		 GL_STATIC_DRAW);
    glVertexAttribPointer(vertex_location, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(vertex_location);
  }

  if(!shape->normals.empty()){

    glBindBuffer(GL_ARRAY_BUFFER, result.normal_buffer);
    glBufferData(GL_ARRAY_BUFFER,
		 sizeof(float) * shape->normals.size(),
		 shape->normals.data(),
		 GL_STATIC_DRAW);
    glVertexAttribPointer(normal_location, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(normal_location);
  }

  if(!shape->uv.empty()){
    glBindBuffer(GL_ARRAY_BUFFER, result.uv_buffer);

    glBufferData(GL_ARRAY_BUFFER,
		 sizeof(float) * shape->uv.size(),
		 shape->uv.data(),
		 GL_STATIC_DRAW);
    glVertexAttribPointer(uv_location, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(uv_location);
  }

  if(!shape->tangents.empty()){
    glBindBuffer(GL_ARRAY_BUFFER, result.tangent_buffer);

    glBufferData(GL_ARRAY_BUFFER,
		 sizeof(float) * shape->tangents.size(),
		 shape->tangents.data(),
		 GL_STATIC_DRAW);
    glVertexAttribPointer(tangent_location, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(tangent_location);
  }

  if(!shape->tangents.empty()){
    glBindBuffer(GL_ARRAY_BUFFER, result.bitangent_buffer);
    glBufferData(GL_ARRAY_BUFFER,
		 sizeof(float) * shape->bitangents.size(),
		 shape->bitangents.data(),
		 GL_STATIC_DRAW);
    glVertexAttribPointer(bitangent_location, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(bitangent_location);
  }

  if(!shape->indices.empty()){

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, result.indices_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
	       sizeof(GLuint) * shape->indices.size(),
	       shape->indices.data(),
	       GL_STATIC_DRAW
	       );
  }

  glBindVertexArray(0);
  return result;
}
// ShapeData* cube(glm::vec3 center_position, glm::vec3 side_lengths){}


// ShapeData* Shape::box(glm::vec3 center_position, glm::vec3 side_lengths){


// }
