#pragma once

#include <glm/glm.hpp>

enum LightSource_t { POINT_LIGHT, DIRECTIONAL_LIGHT, SPOT_LIGHT };

class LightSource {
public:

  LightSource(glm::vec3 color, float intensity) {
    color_ = color;
    intensity_ = intensity;
    has_updated_ = false;
  }
  glm::vec3 color() { return color_; }
  float intensity() { return intensity_; }
  LightSource_t type() { return type_; }
  bool has_updated() { return has_updated_; }
  
protected:
  
  LightSource_t type_;
  bool has_updated_;
  
private:
  glm::vec3 color_;
  float intensity_;
};

class PointLightSource : public LightSource {
public:
  PointLightSource( glm::vec3 position, glm::vec3 color, float intensity );
  glm::vec3 position() { return position_; }
private:
  glm::vec3 position_;
};

class DirectionalLightSource : public LightSource {
public:
  DirectionalLightSource( glm::vec3 direction, glm::vec3 color, float intensity );
  glm::vec3 direction() { return direction_; }
private:
  glm::vec3 direction_;
};


