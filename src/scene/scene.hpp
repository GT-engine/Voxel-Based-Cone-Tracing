#pragma once

#include "world_data.hpp"
#include "../util/gl/texture_store.hpp"
#include "light_source.hpp"
#include "scene_object.hpp"
#include "../util/gl/shapes.hpp"

class Scene{
private:
  std::string error_msg_;
  std::vector<PointLightSource> point_light_sources_;
  std::vector<DirectionalLightSource> directional_light_sources_;

  std::vector<SceneObject*> static_objects_;
  std::vector<SceneObject*> dynamic_objects_;

  bool word_vaos_loaded_ = false;
  World_Data* world_data_ = nullptr;

  bool load_world(TextureStore& tex_store);
  Texture2D* create_and_store_texture(TextureStore& tex_store, RawTexture texture, std::string texture_name);

  bool has_changes_ = true;

public:

  Scene(std::string world_object_file);
  std::string get_error_msg(){return error_msg_;}

  bool init(TextureStore& tex_store);
  std::vector<SceneObject*> get_static_objects() {return static_objects_;}
  std::vector<SceneObject*> get_dynamic_objects(){return dynamic_objects_;}

  bool has_changes(){ return has_changes_;}
  void set_has_changes(bool v){ has_changes_ = v;}

  void add_light_source(LightSource ls);
  void add_dyamic_object(SceneObject*);

  void update_objects();
};
