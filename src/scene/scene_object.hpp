#pragma once
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
#include <functional>
#include <vector>

#include "../util/gl/texture2d.hpp"

class SceneObject {
public:
    enum TextureType {
    DIFFUSE_TEXTURE,
    SPECULAR_TEXTURE,
    NORMALS_TEXTURE,
    NUM_TEXTURE_TYPES
  };
private:

  Texture2D* textures_[NUM_TEXTURE_TYPES] = {0};

  GLuint vao_;
  GLint num_indices_;

  glm::vec3 x_dir = glm::vec3(1,0,0);
  glm::vec3 y_dir = glm::vec3(0,1,0);
  glm::vec3 z_dir = glm::vec3(0,0,1);

  glm::mat4 rotation_mat_ = glm::mat4(1);
  glm::mat4 scale_mat_ = glm::mat4(1);
  glm::mat4 translation_mat_ = glm::mat4(1);

  glm::mat4 model_mat_;

  bool model_is_modified_ = false;
  void modified_mat();

  glm::vec3 emission_ = glm::vec3(0);

  // Stores behaviours in the dynamic object, if the function returns
  // false it should be removed from the behaviour list
  std::vector<std::function<bool(SceneObject*)>> behaviours_;



public:

  SceneObject(GLuint vao, GLint num_indices);


  void set_texture(TextureType type, Texture2D* texture);

  glm::mat4 get_rotation_mat(){return rotation_mat_;}
  glm::mat4 get_translation_mat(){return translation_mat_;}
  glm::mat4 get_scale_mat(){return scale_mat_;}
  glm::vec3 getEmission() { return emission_; }

  glm::vec3 setEmission(glm::vec3 emission) { emission_ = emission; }

  glm::mat4 get_model_mat();
  void update_model_mat();

  void global_rotate(float angle, glm::vec3 pivot);
  void local_rotate(float angle, glm::vec3 pivot);
  void rotate(float angle, glm::vec3 v);
  void rotate_x(float angle);
  void rotate_y(float angle);
  void rotate_z(float angle);

  void move_to(glm::vec3);
  void move_to(float x, float y, float z);

  void translate(glm::vec3);
  void translate(float x, float y, float z);
  void translate_x(float s);
  void translate_y(float s);
  void translate_z(float s);

  void scale(glm::vec3);
  void scale(float x, float y, float z);
  void scale_x(float x);
  void scale_y(float y);
  void scale_z(float z);

  void size(glm::vec3);
  void size(float x);
  void size(float x, float y, float z);

  void bind_vao();
  void bind_texture(TextureType type, GLuint location);
  void bind_textures(GLuint binds[NUM_TEXTURE_TYPES]);
  void draw();


  void run_behaviours();

  // Returns an int of the behaviours
  void add_behaviour(std::function<bool(SceneObject*)>);

  void clear_behaviours();
};
