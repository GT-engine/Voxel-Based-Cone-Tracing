#include "world_data.hpp"

World_Data::World_Data(std::string filename) {
  Assimp::Importer importer;

  auto pp_flags = aiProcess_Triangulate | aiProcess_CalcTangentSpace;
  scene_ = importer.ReadFile(filename, pp_flags);
  if ( scene_ == NULL ) {
    std::cerr << "Failed to read scene: " << filename << std::endl;
    exit(1);
  }

  numMeshes_ = scene_->mNumMeshes;
  numTextures_ = scene_->mNumTextures;
  numMaterials_ = scene_->mNumMaterials;

  for (int i = 0; i < numMaterials_; i++) {
    std::string filepath = filename.substr(0, 1 + filename.rfind("/", filename.length()));
    materials_.push_back(Material(scene_->mMaterials[i], filepath));
  }

  for (int i = 0; i < numMeshes_; i++) {
    meshes_.push_back(Mesh(scene_->mMeshes[i]));
  }
}
