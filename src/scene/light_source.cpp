#include "light_source.hpp"

PointLightSource::PointLightSource( glm::vec3 position, glm::vec3 color, float intensity ) :
  LightSource(color, intensity) {

  type_ = POINT_LIGHT;
  position_ = position;
}
