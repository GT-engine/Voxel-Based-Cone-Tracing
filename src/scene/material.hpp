#pragma once

#include <assimp/scene.h>
#include <cstdint>

enum texture_t {DIFFUSE_TEXTURE, SPECULAR_TEXTURE, AMBIENT_TEXTURE, EMISSIVE_TEXTURE,
		HEIGHT_TEXTURE, NORMALS_TEXTURE, SHININESS_TEXTURE, OPACITY_TEXTURE,
		DISPLACEMENT_TEXTURE, texture_t_n};

typedef struct {
  int width;
  int height;
  int bytes_per_pixel;
  uint8_t* buffer;
} RawTexture;

class Material {
public:
  Material(aiMaterial* material, std::string textures_path, bool windows = false);
  ~Material();
  Material(const Material& m);
  Material& operator=(const Material& material);

  bool hasTexture(texture_t type);
  RawTexture getTexture(texture_t type);
  std::string get_texture_name(texture_t type){
    return texture_names_[type];
  }

  static std::string getTextureTypeString(texture_t type);
private:
  RawTexture texture_buffer_[texture_t_n];
  std::string texture_names_[texture_t_n];

  RawTexture loadImage(const char* filename);
};
