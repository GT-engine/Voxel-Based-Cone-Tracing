#pragma once

#include <iostream>
#include <vector>

#include <assimp/cimport.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "mesh.hpp"
#include "material.hpp"

/* Scene loading class */
class World_Data {
public:
  /* The file should be able to be of all formats supported by assimp */
  World_Data(std::string filename);

  bool has_materials();

  std::vector<Mesh>& get_meshes() { return meshes_; }
  std::vector<Material>& get_materials() { return materials_; }

private:
  const struct aiScene* scene_;
  std::vector<Mesh> meshes_;
  std::vector<Material> materials_;

  unsigned int numTextures_;
  unsigned int numMeshes_;
  unsigned int numMaterials_;

  void createObjects();
};
