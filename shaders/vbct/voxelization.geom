#version 420 core
#extension GL_EXT_geometry_shader : enable

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec2 vert_uv[];
in vec3 vert_normal[];
in vec3 vert_position[];

out vec2 geom_uv;
out vec3 geom_normal;
out vec3 geom_position;



void main(){
  vec3 p1 = gl_PositionIn[0].xyz;
  vec3 p2 = gl_PositionIn[1].xyz;
  vec3 p3 = gl_PositionIn[2].xyz;

  vec3 projs = abs(cross(p2 - p1, p3 - p1));


  for(int i = 0; i < 3; i++){
    geom_uv = vert_uv[i];
    geom_normal = vert_normal[i];
    geom_position = vert_position[i];

    vec3 p = (p1 + (gl_PositionIn[i].xyz - p1));
    if(projs.x > projs.y && projs.x > projs.z){
      gl_Position = vec4(p.yz,0,1);
    } else if (projs.y > projs.z){
      gl_Position = vec4(p.xz, 0,1);
    } else {
      gl_Position = vec4(p.zy, 0,1);
    }
    EmitVertex();
  }
  EndPrimitive();
}
