#version 420 core

in vec2 vertex_uv;
in vec4 vertex_normal;
in vec4 world_possition;

out vec4 color;

uniform sampler2D diffuse;
uniform sampler2D albedo;
uniform sampler2D normal_map;
uniform sampler2D specular;
uniform sampler3D voxel_field;

uniform vec3 col;
uniform int hasTexture;

void main(){
  if (hasTexture == 0) {
    color = vec4(col, 1.0);
  } else {
    vec4 c = texture(diffuse, vertex_uv);
    if (c.a < 0.1) discard;
    color = vec4(c);
  }
}
