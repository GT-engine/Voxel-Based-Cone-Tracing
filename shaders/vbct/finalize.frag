#version 450 core

#define PI 3.14159265359

out vec4 color;

in vec2 vert_position;


uniform sampler2D tex_position;
uniform sampler2D tex_normal;
uniform sampler2D tex_albedo_spec;
uniform sampler3D voxel_field;
uniform sampler2D tex_shadow_map;
uniform sampler2D tex_emission;


uniform mat4 light_projection;
uniform mat4 voxel_projection;
uniform vec3 camera_position;

uniform bool useAO;

ivec3 to_voxel_coord(vec3 pos){
  pos /= 2048.0;
  pos = pos * 0.5 + 0.5;
  return ivec3(pos*512);
}


bool is_outside(ivec3 pos){
  return (pos.x > 512 || pos.y > 512 || pos.z > 512 || pos.x < 0 || pos.y < 0 || pos.z < 0);
}

float calculate_step(vec3 pos, vec3 r, float level){
  //vec3 cube_space = voxel_projection * pos;
  float cube_side =  2 * 2048 / (512.0 / pow(2,level));
  vec3 cube_coord = mod(pos, cube_side);

  float t = 1337;

  for(int i = 0; i < 3; i++){
    float cs = r[i] < 0 ? 0 : cube_side;
    float dt = (cs - cube_coord[i]) / r[i];
    t = min(t, dt);
  }
  return t;
}

vec4 sample_step(vec3 pos, vec3 r, int max_level, out float t){
  vec4 value;
  for(int level = max_level; level >= 0; level--){
    vec3 texture_coord = pos / (2048 * 2) + 0.5;
    value = textureLod(voxel_field, texture_coord, level);

    if(value == vec4(0)){
      t = calculate_step(pos, r, float(level));
      return vec4(0);
    }

  }
  t = calculate_step(pos, r, 0.0);
  return value;
}

vec4 full_voxel_trace(vec3 world_pos, vec3 camera_pos, vec3 normal, int max_level){
  vec3 incomming = world_pos - camera_pos;
  vec3 reflection = incomming - 2 * dot(incomming, normal) * normal;

  vec3 ray = normalize(reflection);

  vec3 pos = world_pos;
  float plane_escape_step = calculate_step(pos, normal, 0);
  pos += (plane_escape_step +0.1) * normal;

  vec3 cur_pos = pos;
  float t = 0;
  for(;;){
    ivec3 voxel_coord = to_voxel_coord(cur_pos);
    if(is_outside(voxel_coord)){
      return vec4(0);
    }
    vec4 voxel = sample_step(cur_pos, ray, max_level, t);
    if(voxel != vec4(0)){
      return voxel;
    }
    cur_pos += (t + 0.1) * ray;
  }
}

float shadow_calculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(tex_shadow_map, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
    float bias = 0.05;
    float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

    return shadow;
}

#define NUM_DIFFUSE_CONES 6
vec3 ConeVectors[NUM_DIFFUSE_CONES] = { vec3(0, 1, 0),
					vec3(0, 0.5, 0.866025),
					vec3(0.823639, 0.5, 0.267617),
					vec3(0.509037, 0.5, -0.700629),
					vec3(-0.509037, 0.5, -0.700629),
					vec3(-0.823639, 0.5, 0.267617) };

const float Wi = 3*PI/20;
const float Weights[6] = {PI/4, Wi, Wi, Wi, Wi, Wi};

float calculateAO() {
  const float voxel_size = 4096. / 512.;
  const float tan_theta = tan(PI/6);

  vec3 pos = texture(tex_position, vert_position).rgb;
  vec3 normal = texture(tex_normal, vert_position).xyz;

  vec3 tangent = normalize(vec3(1, 1, -(normal.x + normal.y)/normal.z));
  vec3 bitangent = cross(normal, tangent);
  mat3 space = mat3(tangent, bitangent, normal);

  float ao = 0.;

  float lambda = 0.00001;
  for (int i = 0; i < NUM_DIFFUSE_CONES; i++) {

    float step_size = 2.;
    float a = 0.;

    vec3 dir = normalize(space * ConeVectors[i]);

    int count = 0;
    vec3 p = pos;
    float dist = voxel_size * 1.5;
    while (a < 1. && dist < 30.) {
      p = pos + dir * dist;

      float mm_level = min(2 * dist * tan_theta / voxel_size, 5);
      vec4 c = textureLod(voxel_field, pos / 4096 + 0.5, mm_level);

      float size = voxel_size * pow(2, mm_level);
      a += (1. - a) * c.a;
      a /= (1 + dist * lambda);
      //a = 1 - pow(1 - a, step_size / size);

      dist += voxel_size * pow(2., 1 + mm_level);
      step_size = step_size * (1 + tan_theta) / (1 - tan_theta);
    }

    ao += Weights[i] * a;
  }

  return ao / NUM_DIFFUSE_CONES;
}

void main(){
  //  vec2 uv = gl_FragCoord.xy / resolution;
  vec3 world_position = texture(tex_position, vert_position).rgb;
  vec4 albedo_spec = texture(tex_albedo_spec, vert_position);
  float shadow = shadow_calculation(light_projection * vec4(world_position,1.0));
  vec3 normal = texture(tex_normal, vert_position).xyz;

  //vec4 voxel = full_voxel_trace(world_position, camera_position, normal, 4);

  //color = vec4(mix(albedo_spec.rgb, voxel.rgb, 0.2), 1.0);
  //color.rgb *= 0.5 * (1.0 - shadow) + 0.5;
  if (useAO) {
    color = vec4(albedo_spec.xyz - vec3(calculateAO()), 1.);
  } else {
    color = vec4(albedo_spec.xyz, 1.);
    //color = vec4(vec3(1. - calculateAO()), 1.);
  }

  //color =vec4(calculateAO());

  //color = vec4(1);
  //color = vec4(vec3(float(voxel_list.color)),1);
}
