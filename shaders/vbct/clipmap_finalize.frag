#version 450 core

#define PI 3.14159265359

out vec4 color;

in vec2 vert_position;

uniform sampler2D tex_position;
uniform sampler2D tex_tangent;
uniform sampler2D tex_normal;
uniform sampler2D tex_albedo_spec;
uniform sampler2D tex_shadow_map;
uniform sampler2D tex_emission;

uniform sampler3D voxel_field;

uniform mat4 light_projection;
uniform mat4 voxel_projection;
uniform vec3 camera_position;

uniform vec3 camera_up;
uniform vec3 camera_lookat;

uniform bool useAO;
uniform vec2 resolution;

uniform int voxel_resolution;
uniform int clipmap_levels;
uniform int voxel_base_dimension;

const float voxel_size = voxel_base_dimension / voxel_resolution;
vec2 uv = gl_FragCoord.xy / resolution;
vec3 tangent = texture(tex_tangent, uv).xyz * 2. - 1.;

vec4 convRGBA8ToVec4(uint val){
  return  vec4( float( (val&0x000000FF) ),
		float( (val&0x0000FF00) >>8U ),
		float( (val&0x00FF0000) >>16U ),
		float( (val&0xFF000000) >>24U ) );
}

vec4 rotate_angle_axis(float angle, vec3 axis) {
	float sn = sin(angle * 0.5);
	float cs = cos(angle * 0.5);
	return vec4(axis * sn, cs);
}

vec4 qmul(vec4 q1, vec4 q2) {
	return vec4(
		q2.xyz * q1.w + q1.xyz * q2.w + cross(q1.xyz, q2.xyz),
		q1.w * q2.w - dot(q1.xyz, q2.xyz)
	);
}

vec3 rotate_vector(vec3 v, vec4 r) {
	vec4 r_c = r * vec4(-1, -1, -1, 1);
	return qmul(r, qmul(vec4(v, 0), r_c)).xyz;
}


ivec3 to_voxel_coord(vec3 pos){
  pos /= 2048.0;
  pos = pos * 0.5 + 0.5;
  return ivec3(pos*voxel_resolution);
}


bool is_outside(ivec3 pos){
  return (pos.x > 512 || pos.y > 512 || pos.z > 512 || pos.x < 0 || pos.y < 0 || pos.z < 0);
}

float calculate_step(vec3 pos, vec3 r, float level){
  //vec3 cube_space = voxel_projection * pos;
  float cube_side = 0.5 * voxel_base_dimension * pow(2, level) / voxel_resolution;
  vec3 cube_coord = mod(pos, cube_side);

  float t = 1337;

  for(int i = 0; i < 3; i++){
    float cs = r[i] < 0 ? 0 : cube_side;
    float dt = (cs - cube_coord[i]) / r[i];
    t = min(t, dt);
  }
  return t;
}

vec3 sample_step(vec3 pos, vec3 r, int max_level, out float t){
  vec3 pc = pos - camera_position;

  vec3 pca = abs(pc);
  float m = max(pca.x, max(pca.y, pca.z));
  int min_level = max(0, int(ceil(log2(m / (0.5 * voxel_base_dimension)))));

  //min_level = 4;

  vec3 value;
  for(int level = min_level; level <= min_level; level++) {
    vec3 tex_coord = (pc) / (voxel_base_dimension * pow(2, level)) + 0.5;

    //float y = tex_coord.y * 128;
    //tex_coord.y = (y + 130. * level) / (128 * clipmap_levels);
    tex_coord.y = tex_coord.y / clipmap_levels + float(level) / clipmap_levels;

    //value = convRGBA8ToVec4(textureLod(voxel_field, tex_coord, 0).r).xyz / 255;
    value = textureLod(voxel_field, tex_coord, 0).xyz;

    if(value == vec3(0)){
      t = calculate_step(pos, r, float(level));
      return vec3(0);
    }
  }

  t = calculate_step(pos, r, 0);
  return value;
}

vec4 full_voxel_trace(vec3 camera_pos, vec3 ray, int max_level){
  vec3 pos = camera_pos;

  float t = 0;
  for(;;){
    vec3 p = abs(pos - camera_pos);
    if(max(p.x, max(p.y, p.z)) > 0.5 * voxel_base_dimension * pow(2, max_level)) {
      return vec4(1, 0, 1, 1);
    }

    vec3 voxel = sample_step(pos, ray, max_level, t);
    if(voxel != vec3(0)){
      return vec4(voxel, 1);
    }
    pos += (t * 0.5 + 0.05) * ray;
  }
  return vec4(1,0,1,1);
}

float shadow(vec3 normal) {
  vec4 position = light_projection * vec4(texture(tex_position, uv).xyz, 1);
  float a = dot(normal, normalize(vec3(0, 2000, 250)));
  float bias = max(0.05 * (1.0 - a), 0.005);
  vec3 p_l = position.xyz / position.w;
  p_l = p_l * 0.5 + 0.5;

  float acc = 0;
  vec2 texel_sz = 1. / textureSize(tex_shadow_map, 0) * a;
  for (int i = -1; i <= 1; ++i) {
    for (int j = -1; j <= 1; ++j) {
      float map_depth = texture(tex_shadow_map, p_l.xy + vec2(i, j) * texel_sz).r;
      acc += p_l.z - bias > map_depth ? 0. : 1.;
    }
  }

  return acc / 9.;
}

// #define NUM_DIFFUSE_CONES 6
// vec3 ConeVectors[NUM_DIFFUSE_CONES] = { vec3(0, 1, 0),
//                                         vec3(0, 0.5, 0.866025),
//                                         vec3(0.823639, 0.5, 0.267617),
//                                         vec3(0.509037, 0.5, -0.700629),
//                                         vec3(-0.509037, 0.5, -0.700629),
//                                         vec3(-0.823639, 0.5, 0.267617) };



// const float Wi = 3*PI/20;
// const float Weights[6] = {PI/4, Wi, Wi, Wi, Wi, Wi};

#define NUM_DIFFUSE_CONES 5
const vec3 ConeVectors[5] = vec3[5](
				    vec3(0.0, 0.0, 1.0),
				    vec3(0.0, 0.707106781, 0.707106781),
				    vec3(0.0, -0.707106781, 0.707106781),
				    vec3(0.707106781, 0.0, 0.707106781),
				    vec3(-0.707106781, 0.0, 0.707106781)
				       );
const float Weights[5] = float[5]( 0.28, 0.18, 0.18, 0.18, 0.18 );
const float Apertures[5] = float[5]( /* tan(45) */ 1.0, 1.0, 1.0, 1.0, 1.0 );

vec3 calc_voxel_index(vec3 pos, int mm_level) {
  vec3 voxel_index = pos / (voxel_base_dimension * pow(2, mm_level)) + 0.5;
  voxel_index = clamp(voxel_index, 0, 1);
  voxel_index.y = voxel_index.y / clipmap_levels + float(mm_level) / clipmap_levels;
  return voxel_index;
}

vec4 linear_voxel_sample(vec3 pos, float mm_level) {
  int low = int(floor(mm_level));
  int high = int(ceil(mm_level));

  float m = exp2(float(low)) * voxel_base_dimension / voxel_resolution;
  vec3 offset = mod(camera_position, m);

  vec4 low_c = texture(voxel_field, calc_voxel_index(pos + offset, low));
  if (low == high)
    return low_c;
  offset = mod(camera_position, m * 2);
  vec4 high_c = texture(voxel_field, calc_voxel_index(pos + offset, high));

  return mix(low_c, high_c, fract(mm_level));
}

float calc_min_level(float dist) {
  float min_level = ceil(max(0, log2((dist) / (voxel_base_dimension * 0.5))));
  float len_to_next = abs(dist - voxel_base_dimension * exp2(min_level) * 0.5);
  float bias = 200.;
  return mix(min_level + 1, min_level, clamp(len_to_next / bias, 0, 1));
}

vec4 trace_cone(vec3 pos, vec3 dir, float offset, float angle, float max_len) {
  dir = normalize(dir);
  float tan_theta = tan(angle / 2);

  float step_size = voxel_size;
  float dist = 0;
  vec4 c = vec4(0);
  float diameter = voxel_size;
  float cur_seg_len = voxel_size;
  pos += dir * offset;

  float occlusion_decay = 0.001;

  float start_level = calc_min_level(length(pos));

  float a = 0;
  while (a < 1. && dist < max_len) {
    vec3 p = pos + dist * dir;

    float mm_level = log2(diameter / voxel_size);
    mm_level = max(start_level, max(mm_level, calc_min_level(length(p))));
    mm_level = min(mm_level, clipmap_levels - 1);

    vec4 c2 = linear_voxel_sample(p, mm_level);

    float size = voxel_size * exp2(mm_level);
    float correction = cur_seg_len / size;

    c2.w = clamp(1. - pow(1. - c2.w, correction), 0, 1);
    c2.rgb *= correction;

    c += clamp((1 - c.a) * c2, 0, 1);
    a +=  (1. - a) * c2.w / (1. + (dist + size) * occlusion_decay);

    float s_last = dist;
    dist += 0.4 * max(diameter, voxel_size);
    cur_seg_len = (dist - s_last);
    diameter = dist * 2 * tan_theta;
  }

  return vec4(c.rgb, a);
}

vec3 diffuse_cones(vec3 pos) {
  pos -= camera_position;

  vec3 normal = texture(tex_normal, vert_position).xyz;
  vec3 bitangent = cross(tangent, normal);
  mat3 space = mat3(tangent, bitangent, normal);
  float m = calc_min_level(length(pos));

  float sz = exp2(m) * voxel_size;
  vec3 c = vec3(0);
  for (int i = 0; i < NUM_DIFFUSE_CONES; i++) {
    vec3 dir = space * normalize(ConeVectors[i]);

    c += Weights[i] * trace_cone(pos, dir, sz, PI / 2., 2000.).xyz;
  }
  return c;// / (NUM_DIFFUSE_CONES * 0.5);
}

vec3 trace_specular(vec3 pos, vec3 normal, vec3 camera_pos) {
  vec3 dir = normalize(reflect(normalize(pos - camera_pos), normal));
  pos = pos - camera_pos;

  float offset = (exp2(calc_min_level(length(pos)))) * voxel_size;
  //pos += normal * offset;
  vec3 color = trace_cone(pos, dir, offset, PI * 2. / 45., 2000.).xyz;

  return color;
}

float calculateAO(vec3 pos) {
  pos -= camera_position;

  vec3 normal = texture(tex_normal, vert_position).xyz;
  vec3 bitangent = cross(tangent, normal);
  mat3 space = mat3(tangent, bitangent, normal);
  float m = calc_min_level(length(pos));

  float sz = exp2(m) * voxel_size;
  float a = 0;
  for (int i = 0; i < NUM_DIFFUSE_CONES; i++) {
    vec3 dir = space * normalize(ConeVectors[i]);
    a += Weights[i] * trace_cone(pos, dir, sz , PI / 2., 30.).w;
  }
  return 1. - a / NUM_DIFFUSE_CONES;
}

void main(){

  vec3 world_position = texture(tex_position, vert_position).rgb;
  vec4 albedo_spec = texture(tex_albedo_spec, vert_position);
  vec3 normal = texture(tex_normal, vert_position).xyz;

  vec3 L = vec3(0., 300., 0.) - world_position;
  float light_distance = length(L);
  L = normalize(L);
  float NdotL = dot(normal, L);
  float intensity = max(NdotL, 0.1);
  vec3 c = albedo_spec.xyz * intensity * vec3(1.) * 2;

  vec3 V = normalize(camera_position - world_position);
  vec3 H = normalize(V + L);
  c += albedo_spec.a * albedo_spec.xyz * pow(max(dot(H, normal), 0.), 4.);

  c /= light_distance / 10. + 1;

  c = albedo_spec.xyz * max(0., dot(normal, normalize(vec3(0, 2000, 250))));
  c *= max(shadow(normal), 0.);

  //color.rgb *= 0.5 * (1.0 - shadow) + 0.5;
  // if (useAO) {
  //   //vec4 voxel = full_voxel_trace(world_position, camera_position, normal, 5);

  //   // color = vec4(mix(c, voxel.rgb, 1.), 0.9);
  //   // vec2 uv = gl_FragCoord.xy / resolution;
  //   // uv = uv * 2.0 - 1.0;
  //   // vec3 span_x = cross(camera_lookat, camera_up);
  //   // vec3 plane_coord = span_x * uv.x + camera_up * uv.y;
  //   // plane_coord += camera_position + camera_lookat;
  //   // vec3 r = normalize(plane_coord - camera_position);

  //   // vec4 voxel = full_voxel_trace(camera_position, r, clipmap_levels - 1);
  //   // color = vec4(voxel.xyz, 1.);

  //   color = mix(vec4(albedo_spec.xyz, 1), color, 0.);
  //   //color.xyz = c;
  //   color.xyz = diffuse_cones(world_position) * albedo_spec.rgb;
  // } else {
  color = vec4(trace_specular(world_position, normal, camera_position) * albedo_spec.a, 1);
  color.xyz += c * calculateAO(world_position);
  color.xyz += albedo_spec.xyz * diffuse_cones(world_position);
  color.xyz += texture(tex_emission, vert_position).xyz;

    //color = vec4(diffuse_cones(world_position), 1);
    //color = vec4(vec3(calculateAO(world_position)), 1.);
    //}


  float gamma = 2.2;
  color.rgb = pow(color.rgb, vec3(1.0/gamma));
}
