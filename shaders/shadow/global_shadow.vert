#version 450 core

layout (location = 0) in vec3 vertex;

uniform mat4 light_space_matrix;

void main(){
  gl_Position = light_space_matrix * vec4(vertex, 1.0);
}
