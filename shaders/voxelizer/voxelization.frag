#version 450 core


#define MAX_POINT_LIGHT 10
#define MAX_DIRECTIONAL_LIGHT 5

struct PointLight_t {
  vec3 position; //offset 0
  vec3 color; //offset 16
  float intensity; //offset 32
};

struct DirectionalLight_t {
  vec3 direction; //offset 0
  vec3 color; //offset 16
  float intensity; //offset 32
};

/*
layout (std140) uniform PointLightBlock {
  PointLight_t PointLights[MAX_POINT_LIGHT];
};
uniform int PointLightCount;

layout (std140) uniform DirectionalLightBlock {
  DirectionalLight_t DirectionalLights[MAX_POINT_LIGHT];
};
uniform int DirectionalLightCount;
*/

layout (rgba8) uniform coherent volatile image3D voxel_field;

uniform sampler2D diffuse;
uniform sampler2D albedo;
uniform sampler2D normal_map;
uniform sampler2D specular;
uniform sampler2D shadow_map;

in vec2 geom_uv;
in vec3 geom_normal;
in vec3 geom_position;
in vec4 geom_light_position;

vec4 diffuse_c  = texture(diffuse, geom_uv);
vec4 albedo_c   = texture(albedo, geom_uv);
vec4 normal_c   = texture(normal_map, geom_uv);
vec4 specular_c = texture(specular, geom_uv);

float shadow_calculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadow_map, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
    float bias = 0.01;
    float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

    return shadow;
}



vec3 calculate_light(vec3 light_dir, vec3 light_color, float light_intensity,
		     bool attenuation) {
  float light_distance = length(light_dir);

  float NdotL = dot(geom_normal, normalize(light_dir));
  float intensity = max(NdotL, 0.);
  vec3 color = albedo_c.xyz * intensity * light_color * light_intensity;

  if (attenuation)
    color /= light_distance + 1;

  return color;
}

vec3 shade() {
  vec3 color = vec3(0);

  const int DirectionalLightCount = 1;
  const int PointLightCount = 1;

  DirectionalLight_t DirectionalLights[DirectionalLightCount];
  DirectionalLights[0] = DirectionalLight_t(vec3(.2, -1, 0),
					    vec3(1.),
					    1.);


  PointLight_t PointLights[PointLightCount];
  PointLights[0] = PointLight_t(vec3(0, 2, 0), vec3(1), 10.);

  for (int i = 0; i < PointLightCount; i++) {
    PointLight_t ls = PointLights[i];
    vec3 light_dir = ls.position - geom_position;
    color += calculate_light(light_dir, ls.color, ls.intensity, true);
  }

  return color;
  for (int i = 0; i < DirectionalLightCount; i++) {
    DirectionalLight_t ls = DirectionalLights[i];
    vec3 light_dir = -ls.direction;
    color += calculate_light(light_dir, ls.color, ls.intensity, false);
  }

  return color;
}

void main(){
  ivec3 image_size = imageSize(voxel_field);

  vec4 res  = vec4(shade(), 1.0);

  float shadow = shadow_calculation(geom_light_position);
  res.rgb *= 0.5 * (1.0 - shadow) + 0.5;

  imageStore(voxel_field,
	     ivec3(image_size * (0.5 * geom_position + 0.5)),
	     res);
}
