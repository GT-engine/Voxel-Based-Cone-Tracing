#version 420 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

out vec2 vert_uv;
out vec3 vert_normal;
out vec3 vert_position_proj;
out vec3 vert_position;
out vec4 vert_position_light_space;

uniform mat4 projection;
uniform mat4 model;
uniform mat4 shadow_space;

void main(){
  vert_uv = uv;
  vert_normal = normal;
  vert_position = (model * vec4(vertex, 1)).xyz;
  //vert_position = vertex;
  vec4 position = projection * vec4(vert_position, 1);
  vert_position_proj = position.xyz;

  vert_position_light_space = shadow_space * vec4(vert_position, 1);

  gl_Position = position;
}
