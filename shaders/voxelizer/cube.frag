#version 420 core

in vec3 cube_color;
out vec4 color;

void main(){
  color = vec4(cube_color, 1.);
}
