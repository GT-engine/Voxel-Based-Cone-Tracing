#version 460 core

in vec2 vert_pos;

// For octree nodes the header contains a pointer
struct OctreeNode {
  uint header;
  uint neighbor[6];
  uint parent_ptr;
};
layout (std430, binding = 0) coherent volatile buffer octree_node_store {
  OctreeNode octree_nodes[];
};

const float voxel_size_fract = 1. / 8.;

uniform sampler2D depth_map;

uniform mat4 light_proj_inverse;
uniform mat4 voxel_projection;
uniform int voxel_span;

const uint STATIC_BIT = 1 << 31;
const uint DYNAMIC_CURRENT_BIT = 1 << 30;
const uint DYNAMIC_PREV_BIT = 1 << 29;
const uint LIGHT_CURRENT_BIT = 1 << 28;
const uint LIGHT_PREV_BIT = 1 << 27;
// During construction the LIGHT_CURRENT_BIT is used as a mutex
// bit. This shouldn't make any deadlocks as light should be injected
// AFTER the construction stage and if on should be flipped off after
// filering.
const uint LOCK_BIT = LIGHT_CURRENT_BIT;
const uint CHILD_INDEX_BITS = 0x07FFFFFF;


ivec3 voxel_octant_vec(ivec3 voxel_coord, uint level_space_size){
  return voxel_coord / int(level_space_size / 2);
}
uint octant_vec_to_idx(ivec3 octant_vec){
  ivec3 t = octant_vec << ivec3(2,1,0);
  return t.x | t.y | t.z;
}

uint octree_node_child_idx(uint location){
  return octree_nodes[location].header & CHILD_INDEX_BITS;
}
ivec3 to_voxel_coord(vec3 pos){
  return ivec3(floor(pos * (1/8.) + 256.));
}


struct FindResult {
  bool found;
  uint parent_ptr;
  uint node_ptr;
  int level;
};
FindResult find_node(vec3 pos, int target_level){
  FindResult res = FindResult(false, 0,0, 9);
  ivec3 voxel_coord = ivec3(floor(pos * voxel_size_fract + 256.));
  int dim = 512;
  for(res.level = 9; res.level > (target_level + 1); res.level--){
    octree_nodes[res.node_ptr].header |= LIGHT_CURRENT_BIT;
    ivec3 octant_vec = voxel_octant_vec(voxel_coord, dim);
    uint child_idx = octant_vec_to_idx(octant_vec);
    uint child_start_ptr = octree_node_child_idx(res.node_ptr);
    uint child_ptr = child_start_ptr + child_idx;
    if(octree_nodes[child_ptr].header == 0){
      // The requested node doesn't exist
      return res;
    }
    res.parent_ptr = res.node_ptr;
    res.node_ptr = child_ptr;
    dim /= 2;
    voxel_coord -= dim * octant_vec;
  }
  res.parent_ptr = res.node_ptr;
  //  dim /= 2;
  ivec3 child_octant_vec = voxel_octant_vec(voxel_coord, dim);
  uint child_idx = octant_vec_to_idx(child_octant_vec);
  uint child_start_ptr = octree_node_child_idx(res.node_ptr);
  uint voxel_ptr = child_start_ptr + child_idx;
  if(octree_nodes[voxel_ptr].header == 0){
    return res;
  }
  res.found = true;
  res.node_ptr = voxel_ptr;
  res.level--;
  return res;
}


void main(){
  float depth = texture(depth_map, vert_pos.xy * 0.5 + 0.5).r;
  depth = (depth - 0.5) * 2;
  vec4 light_coord =  vec4(vert_pos.x, vert_pos.y, depth, 1.0);
  vec4 world_coord = light_proj_inverse * light_coord;

  FindResult fr = find_node(world_coord.xyz, 0);
  if(fr.found){
    octree_nodes[fr.node_ptr].neighbor[1] = 0xFFFFFFFF;
  }
}
