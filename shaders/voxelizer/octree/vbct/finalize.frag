#version 450 core

#define PI 3.14159265359

struct OctreeNode {
  uint header;
  uint neighbor[6];
  uint color_ui;
};
layout (std430, binding = 0) readonly buffer octree_node_store {
  OctreeNode octree_nodes[];
};

out vec4 color;
in vec2 vert_position;

uniform sampler3D voxel_bricks;
uniform sampler2D tex_position;
uniform sampler2D tex_normal;
uniform sampler2D tex_albedo_spec;
uniform sampler2D tex_shadow_map;
uniform sampler2D tex_tangent;

uniform mat4 light_projection;
uniform mat4 voxel_projection;
uniform vec3 camera_position;

uniform int voxel_span;

uniform bool useAO;

uniform ivec3 voxel_bricks_dims;

const vec3 texel_size = 1. / (3. * vec3(voxel_bricks_dims));
const vec3 brick_sample_offset = texel_size * 0.55;

const float voxel_size = 4096. / voxel_span;
const float voxel_size_fract = 1. / voxel_size;

/**
 *   Cachend functions that are commonly used
 **/
const float level_spans[10] = {1.0,2.0,4.0,8.0,16.0,32.0,64.0,128.0,256.0,512.0};
const float cube_sides[10] = {8.0,16.0,32.0,64.0,128.0,256.0,512.0,1024.0,2048.0,4096.0};
const float cube_sides_invs[10] = {0.125,6.25e-2,3.125e-2,1.5625e-2,7.8125e-3,3.90625e-3,
				   1.953125e-3,9.765625e-4,4.8828125e-4,2.44140625e-4};

vec3 world_position = texture(tex_position, vert_position).rgb;
vec4 albedo_spec = texture(tex_albedo_spec, vert_position);
vec3 normal = texture(tex_normal, vert_position).xyz;
vec3 tangent = texture(tex_tangent, vert_position).xyz * 2. - 1.;
vec3 bitangent = cross(tangent, normal);
/**
 * OCTREE METHODS
 *
 **/
ivec3 octree_node_brick_coord(uint node_idx){
  node_idx -= 1; // Ignoring the root node idx
  const ivec3 D = voxel_bricks_dims;
  return 3 * ((ivec3(node_idx) / ivec3(8, 8*D.x, 8*D.x*D.y)) % ivec3(D.x, D.y,D.z));
}

uint octree_node_child_idx(uint location){
  return octree_nodes[location].header & 0x07FFFFFF;
}
bool octree_node_constructed(uint location){
  return octree_node_child_idx(location) != 0;
}

ivec3 voxel_octant_vec(ivec3 voxel_coord, uint level_space_size){
  return voxel_coord / int(level_space_size / 2);
}

uint octant_vec_to_idx(ivec3 octant_vec){
  ivec3 t = octant_vec << ivec3(2,1,0);
  return t.x | t.y | t.z;
}

vec4 uint32_to_rgba8(uint v){
  return vec4((v >> uvec4(24,16,8,0)) & 255);
}

bool is_outside(ivec3 pos){
  return (pos.x > voxel_span || pos.y > voxel_span || pos.z > voxel_span || pos.x < 0 || pos.y < 0 || pos.z < 0);
}

ivec3 to_voxel_coord(vec3 pos, int level){
  return ivec3(floor(pos * voxel_size_fract + 256.) / exp2(level)) ;
}

ivec3 idx_to_octant_vec(uint n){
  return (ivec3(n) >> ivec3(2,1,0)) & 1;
}
uint sibling_start(uint node_ptr){
   return node_ptr - ((node_ptr - 1) % 8);
}
uint octant_idx(uint node_ptr){
  return node_ptr - sibling_start(node_ptr);
}
ivec3 ptr_to_octant_vec(uint node_ptr){
  return idx_to_octant_vec(octant_idx(node_ptr));
}

vec3 brick_sample_point(vec3 pos, int level){
  return mod(pos, cube_sides[level]) * cube_sides_invs[level];
}

// Samples the brick on an internal cordinate corresponding to the
// world position
vec4 sample_brick(uint brick_ptr, vec3 pos, int level){
  vec3 sample_point = brick_sample_point(pos,level);
  sample_point *= 1.9 * texel_size;
  vec3 brick_start_coord = vec3(octree_node_brick_coord(brick_ptr)) * texel_size;
  vec3 sample_position = brick_start_coord + brick_sample_offset + sample_point;
  return texture(voxel_bricks, sample_position);
}

vec4 linear_sample_brick(uint node_brick, uint parent_brick,
			  vec3 pos, int lower_level, float frac ){
  vec4 low_sample = sample_brick(node_brick, pos, lower_level);
  vec4 high_sample = sample_brick(parent_brick, pos, lower_level +1);
  return mix(low_sample, high_sample, frac);
}



float calculate_step(vec3 pos, vec3 r, vec3 r_inv, uint level){
  float cube_side =  cube_sides[level];
    //  float cube_side =  2 * 2048 / (voxel_span / pow(2,level));
  vec3 cube_coord = mod(pos, cube_side);
  float t = 1337;
  for(int i = 0; i < 3; i++){
    float cs = r[i] < 0 ? 0 : cube_side;
    float dt = (cs - cube_coord[i]) * r_inv[i];
    t = min(t, dt);
  }
  return t;
}

float calculate_mid_step(vec3 pos, vec3 dir, vec3 dir_inv, int level){
  float t0 = calculate_step(pos, dir, dir_inv, level);
  float t1 = calculate_step(pos + dir * t0 + 0.1, dir, dir_inv, level);
  return t0 + (t1 - t0) * 0.5;
}

struct FindResult {
  bool found;
  uint parent_ptr;
  uint node_ptr;
  int level;
};
FindResult find_node(vec3 pos, int target_level){
  FindResult res = FindResult(false, 0,0, 9);
  ivec3 voxel_coord = ivec3(floor(pos * voxel_size_fract + 256.));
  int dim = 512;
  for(res.level = 9; res.level > (target_level + 1); res.level--){
     ivec3 octant_vec = voxel_octant_vec(voxel_coord, dim);
     uint child_idx = octant_vec_to_idx(octant_vec);
     uint child_start_ptr = octree_node_child_idx(res.node_ptr);
     uint child_ptr = child_start_ptr + child_idx;
     if(octree_nodes[child_ptr].header == 0){
      // The requested node doesn't exist
      return res;
     }
     res.parent_ptr = res.node_ptr;
     res.node_ptr = child_ptr;
     dim /= 2;
     voxel_coord -= dim * octant_vec;
  }
  res.parent_ptr = res.node_ptr;
  //  dim /= 2;
  ivec3 child_octant_vec = voxel_octant_vec(voxel_coord, dim);
  uint child_idx = octant_vec_to_idx(child_octant_vec);
  uint child_start_ptr = octree_node_child_idx(res.node_ptr);
  uint voxel_ptr = child_start_ptr + child_idx;
  if(octree_nodes[voxel_ptr].header == 0){
    return res;
  }
  res.found = true;
  res.node_ptr = voxel_ptr;
  res.level--;
  return res;
}

struct SampleResult {
  bool found;
  int level;
  vec4 color;
};
SampleResult sample_octree(vec3 pos, float level){
  SampleResult res = SampleResult(false,9,vec4(0));
  // vec3 f_pos = (0.5 * (voxel_projection * vec4(pos,1.)) + 0.5).xyz;
  int target_level = int(floor(level));
  FindResult fr = find_node(pos, target_level);

  if(fr.found || (fr.level == target_level + 1)){
    uint node_brick_ptr = sibling_start(octree_node_child_idx(fr.parent_ptr));
    uint parent_brick_ptr = sibling_start(fr.parent_ptr);
    res.color = linear_sample_brick(node_brick_ptr, parent_brick_ptr,
				    pos, target_level, fract(level));
    res.found = true;

  } else if(fr.level == target_level + 2){
    uint parent_brick_ptr = sibling_start(fr.parent_ptr);
    vec4 parent_color = sample_brick(parent_brick_ptr, pos, target_level+1);
    res.color = mix(vec4(0), parent_color, fract(level));
    //res.color = parent_color * 0.5;
    res.found = true;
  }
  res.level = fr.level;
  return res;
}

// struct Trace {
//   bool out_of_range;
//   bool psuedo_node;

//   float max_range;

//   vec3 dir;
//   uint pos;
//   float dist;

//   float level;
//   int lower_level;

//   uint parent_ptr;
//   uint node_ptr;

//   ivec3 voxel_coord;
//   ivec3 octant_vector;

//   vec4 color_sample;
//   float level_coef;
// };
// void trace_calc_level(inout Trace t){
//   int prev_low_level = t.lower_level;
//   t.level = min(t.dist * level_coef, 7);
//   t.lower_level = int(floor(t.level));
//   int lvl_diff = t.lower_level - prev_low_level;
//   if(lvl_diff  != 0){
//     t.octant_vec   = idx_to_octant_vector(t.node_ptr);
//     t.node_ptr     = t.parent_ptr;
//     t.parent_ptr   = octree_nodes[t.node_ptr].parent_ptr;
//     t.psuedo_node  = false;
//     t.voxel_coord /= exp2(lvl_diff);
//   }
// }
// void trace_next(inout Trace t){
//   // The sampling is allways done in between the next nodes entrance
//   // point and exit point in the trace
//   float sample_step = calculate_mid_step(t.pos, t.dir,t.lower_level);

//   t.pos += dir * sample_step;
//   t.dist += sample_step;
//   if(t.dist >= t.max_dist){
//     t.out_of_range = true;
//     return;
//   }
//   trace_calc_level(t);
//   ivec3 new_voxel_coord = to_voxel_coord(t.pos, t.lower_level);
//   ivec3 voxel_diff = new_voxel_coord - t.voxel_coord;
//   ivec3 location_vector = t.octant_vector + voxel_diff;
//   ivec3 next_octant_vector = location_vector % 2;
//   int neighbor_dir = 0;

//   if(voxel_diff.x != 0){
//     neighbor_dir = voxel_diff.x < 0 ? 1 : 0;
//   } else if (voxel_diff.y != 0){
//     neighbor_dir = voxel_diff.y > 0 ? 2 : 3;
//   } else {
//     neighbor_dir = voxel_diff.z > 0 ? 4 : 5;
//   }

//   // If the level isn't zero we can look for neighbors directly
//   uint neighbor_ptr = 0;
//   if(!t.psuedo_node && t.level != 0) {
//     neighbor_ptr = octree_nodes[t.node_ptr].neighbor[neighbor_dir];
//   } else if(t.psuedo_node || t.level == 0){
//     bool neighbor_is_local = (location_vector & ivec3(~1)) == ivec3(0);
//     if(neighbor_is_local){
//       neighbor_ptr = octree_node_child_idx(t.parent_ptr)
//		   + octant_vec_to_idx(location_vector);
//       if(octree_nodes[neighbor_ptr].header == 0){
//	  neighbor_ptr = 0;
//       }
//     } else {
//       uint pn = octree_nodes[t.parent_ptr].neighbor[neighbor_dir];
//       if(pn != 0){
//	uint parent_neighbor_octant_start = octree_node_child_idx(pn);
//	uint neighbor_octant_idx = octant_vec_to_idx(next_octant_vector);
//	neighbor_ptr = parent_neighbor_octant_start + neighbor_octant_idx;
//	if(octree_nodes[neighbor_ptr].header == 0){
//	  neighbor_ptr = 0;
//	}
//       }
//     }
//   }
//   if(neighbor_ptr != 0){
//     // If the neighbor exists we simply sample that
//     t.psuedo_node = false;
//     t.parent_ptr = octree_nodes[neighbor_ptr].parent_ptr;
//     t.voxel_coord = new_voxel_coord;
//     t.octant_vector = (t.octant_vector + voxel_diff) % 2
//     t.node_ptr = neighbor_ptr;
//   } else {
//     // If the nodes didn't have direct neighbor we might be able to
//     // create a psuedo sample bysampling its brick coord anyhow,
//     // this will give us part of the color of the neighboringnodes
//     uint parent_sibling = octree_nodes[t.parent_ptr].neighbor[neighbor_dir];
//     if(parent_sibling != 0){
//       t.psuedo_node = true;
//       t.voxel_coord = new_voxel_coord
//       t.parent_ptr = parent_sibling;
//     } else {
//       // However if we the parent didn't have a neighbor in that
//       // direction we can skip one parent length worth of
//       // querying. However here we search for the new node using a
//       // top down search of the octree. The reason for this is kind
//       // of arbitary however the guess is that if we do a bottom up
//       // search this could lead to more expensive and more
//       // complicated code anyhow.

//       float sample_step = calculate_mid_step(pos, t.dir, t.low_level + 1);
//       t.dist += sample_step;
//       t.pos += dir * sample_step;
//       if(t.dist >= t.max_dist){
//	t.out_of_range = true;
//	return;
//       }
//       trace_calc_level(t);
//       FindResult fr;
//       do {
//	fr = find_node(pos, t.low_level);
//	if(fr.found) {
//	  t.node_ptr = fr.node_ptr;
//	  t.parent_ptr = octree_nodes[t.node_ptr].parent_ptr;
//	  break;
//	} else if(fr.level == t.low_level+1){
//	  t.psueod_node = true;
//	  t.parent_ptr = fr.node_ptr;
//	  break;
//	} else {
//	  float sample_step = calculate_mid_step(pos, t.dir, fr.level);
//	  t.dist += sample_step;
//	  t.pos += dir * sample_step;
//	  trace_calc_level(t);
//	  if(t.dist >= t.max_dist){
//	    t.out_of_range = true;
//	    return;
//	  }
//	}
//       } while(true);
//     }
//   }
// }


float shadow(vec3 normal) {

  vec4 position = light_projection * vec4(world_position.xyz, 1);
  float bias = max(0.05 * (1.0 - dot(normal, normalize(vec3(0., 1500, 250)))), 0.005);
  vec3 p_l = position.xyz / position.w;
  p_l = p_l * 0.5 + 0.5;

  float acc = 0;
  vec2 texel_sz = 1. / textureSize(tex_shadow_map, 0);
  for (int i = -1; i <= 1; ++i) {
    for (int j = -1; j <= 1; ++j) {
      float map_depth = texture(tex_shadow_map, p_l.xy + vec2(i, j) * texel_sz).r;
      acc += p_l.z - bias > map_depth ? 0. : 1.;
    }
  }

  return acc / 9.;
}


vec4 trace_cone(vec3 pos, vec3 dir, float offset, float angle, float max_dist, float oc){
  oc = 0.001;
  float tan_theta = tan(angle / 2);
  vec3 dir_inv = 1. / dir;
  float step_size = voxel_size;
  float dist = 0;
  vec4 c = vec4(0);
  float diameter = voxel_size;
  float cur_seg_len = voxel_size;
  pos += dir * offset;

  float occlusion_decay = oc;

  float a = 0;
  while (a < 1. && dist <  max_dist) {
    vec3 p = pos + dist * dir;

    float mm_level = log2(diameter * voxel_size_fract);
    mm_level = min(max(0., mm_level), 8.);

    float next_step = 0.3 * max(diameter, voxel_size);
    float s_last = dist;
    SampleResult s = sample_octree(p, mm_level);
    //if(s.found){
      vec4 c2 = s.color;
      //c2.rgb *= 1. + 0.35 * mm_level;
      float size = voxel_size * exp2(mm_level);
      float correction = cur_seg_len / size;

      c2.w = clamp(1. - pow(1. - c2.w, correction), 0 ,1);
      c2.rgb *= correction;

      c += clamp((1 - c.a) * c2, 0, 1);
      a +=  (1. - a) * c2.w /(1. + (dist + size) * occlusion_decay);

      dist += next_step;

    // }  else {
    //   float t = calculate_step(p,dir, dir_inv, max(0,s.level -1)) + 0.5;
    //   dist += max(t, next_step);
    // }
    cur_seg_len = (dist - s_last);
    diameter = dist * 2 * tan_theta;
  }
  return vec4(c.rgb,a);
}

#define NUM_DIFFUSE_CONES 5
const vec3 ConeVectors[5] = vec3[5](
				    vec3(0.0, 0.0, 1.0),
				    vec3(0.0, 0.707106781, 0.707106781),
				    vec3(0.0, -0.707106781, 0.707106781),
				    vec3(0.707106781, 0.0, 0.707106781),
				    vec3(-0.707106781, 0.0, 0.707106781)
				       );
const float Weights[5] = float[5]( 0.28, 0.18, 0.18, 0.18, 0.18 );
const float Apertures[5] = float[5]( /* tan(45) */ 1.0, 1.0, 1.0, 1.0, 1.0 );

vec3 diffuse_cones(vec3 pos) {
  //pos += normal * 1.75 * voxel_size;

  mat3 space = mat3(tangent, bitangent, normal);
  vec3 c = vec3(0);
  for (int i = 0; i < NUM_DIFFUSE_CONES; i++) {
    vec3 dir = space * normalize(ConeVectors[i]);

    c += Weights[i] * trace_cone(pos, dir,  voxel_size * 1.75,
				 PI/2., 2000., 0.1).rgb;
  }
  return c;// / (NUM_DIFFUSE_CONES * 0.5);
}

float calculateAO(vec3 pos) {
  mat3 space = mat3(tangent, bitangent, normal);
  float a = 0;
  for (int i = 0; i < NUM_DIFFUSE_CONES; i++) {
    vec3 dir = space * normalize(ConeVectors[i]);
    a += Weights[i] * trace_cone(pos, dir, voxel_size * 1.75 , PI / 2., 60., 0.01).w;
  }
  return 1. - a / NUM_DIFFUSE_CONES;
}


vec4 point_radiance(vec3 pos){
  FindResult self_r  = find_node(pos, 0);
  vec4 self_color = vec4(0);
  if(self_r.found){
    self_color =  sample_brick(sibling_start(self_r.node_ptr), pos, 0);
  }
  return self_color;
}

void main(){


  // float normal_espace_step = calculate_step(world_position, normal, 0) + 0.2;
  // vec3 escape_pos = world_position + normal * normal_espace_step;
  color = vec4(0,0,0,1);
  // color = almedo_spec.xyz;
  // color.rgb = diffuse_cones(world_position);
  color = albedo_spec * max(0., dot(normal, normalize(vec3(0, 2000, 250))));
  color *= shadow(normal) * calculateAO(world_position);
  //color += point_radiance(world_position);
  vec3 dir = normalize(reflect(world_position - camera_position, normal));
  vec4 spec = trace_cone(world_position, dir, voxel_size * 3.,
			 PI * 2. / 45., 2000., 0.3);
  color.rgb += spec.rgb * albedo_spec.a;
  color.rgb += albedo_spec.rgb * diffuse_cones(world_position);

  color.rgb = pow(color.rgb, vec3(1/2.2));
}
