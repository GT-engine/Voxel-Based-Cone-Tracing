#version 460 core

struct OctreeNode {
  uint header;
  uint neighbor[6];
  uint color_ui;
};
layout (std430, binding = 0) readonly buffer octree_node_store {
  OctreeNode octree_nodes[];
};

uniform sampler3D voxel_bricks;

uniform vec3 camera_pos;
uniform vec3 camera_dir;
uniform vec3 camera_up;
uniform vec3 world_box;

uniform mat4 voxel_projection;

uniform float fov_tan;
uniform float aspect_ratio;
uniform vec2 resolution;

uniform ivec3 voxel_bricks_dims;
uniform int voxel_span;

const vec3 texel_size = 1. / (3. * vec3(voxel_bricks_dims));

const int DISPLAY_LEVEL = 1;
const float cube_sides[10] = {8.0,16.0,32.0,64.0,128.0,256.0,512.0,1024.0,2048.0,4096.0};

out vec4 color;
in vec3 position;

ivec3 octree_node_brick_coord(uint node_idx){
  node_idx -= 1; // Ignoring the root node idx
  const ivec3 D = voxel_bricks_dims;
  return 3 * ((ivec3(node_idx) / ivec3(8, 8*D.x, 8*D.x*D.y)) % ivec3(D.x, D.y,D.z));
}

ivec3 to_voxel_coord(vec3 pos){
  pos /= 2048.0;
  pos = pos * 0.5 + 0.5;
  return ivec3(pos*voxel_span);
}
vec4 uint32_to_rgba8(uint v){
  return vec4((v >> uvec4(24,16,8,0)) & 255);
}
uint octree_node_child_idx(uint location){
  return octree_nodes[location].header & 0x07FFFFFF;
}
bool octree_node_constructed(uint location){
  return octree_node_child_idx(location) != 0;
}

ivec3 voxel_octant_vec(ivec3 voxel_coord, uint level_space_size){
  return voxel_coord / int(level_space_size / 2);
}
uint octant_vec_to_idx(ivec3 octant_vec){
  ivec3 t = octant_vec << ivec3(2,1,0);
  return t.x | t.y | t.z;
}
ivec3 idx_to_octant_vec(uint n){
  return (ivec3(n) >> ivec3(2,1,0)) & 1;
}


// Returns the  node index
struct SampleResult {
  bool found;
  uint parent_ptr;
  uint node_ptr;
  uint level;
  vec4 color;
};
SampleResult sample_octree(vec3 pos, int target_level){
  SampleResult result = SampleResult(false,0,0,9, vec4(0));
  // vec3 f_pos = (0.5 * (voxel_projection * vec4(pos,1.)) + 0.5).xyz;

  // ivec3 voxel_coord = ivec3(512 * f_pos);
  // vec3 brick_coord = 512. * f_pos;

  ivec3 voxel_coord = ivec3(voxel_span * (pos / (2048 * 2) + 0.5));
  vec3 brick_coord = voxel_span * (pos / (2048. * 2.) + 0.5);
  int dim = voxel_span;

  uint brick_start_ptr = 0;
  for(;result.level > target_level; result.level--){
    ivec3 octant_vec = voxel_octant_vec(voxel_coord, dim);
    uint child_idx = octant_vec_to_idx(octant_vec);
    uint child_start_ptr = octree_node_child_idx(result.node_ptr);
    uint child_ptr = child_start_ptr + child_idx;

    if(octree_nodes[child_ptr].header == 0){
      // The requested node doesn't exist
      return result;
    }

    result.parent_ptr = result.node_ptr;
    result.node_ptr = child_ptr;

    dim /= 2;
    voxel_coord -= dim * octant_vec;
    brick_coord -= vec3(dim * octant_vec);
  }

  ivec3 child_octant_vec = voxel_octant_vec(voxel_coord, dim);

  uint child_idx = octant_vec_to_idx(child_octant_vec);
  uint child_start_ptr = octree_node_child_idx(result.node_ptr);
  uint voxel_idx = child_start_ptr + child_idx;
  if(octree_nodes[voxel_idx].header == 0){
    return result;
  }

  const vec3 brick_offset = texel_size * 0.5;

  vec3 brick_start_coord = vec3(octree_node_brick_coord( child_start_ptr))
			 * texel_size;

  // Cheat sample the original voxel centers
  //brick_coord =  2. * texel_size * vec3(voxel_coord) / float(dim);

  // Ajust the brick coord such that it samples from the inner part of
  // the brick that contains the information for the actual voxel.
  brick_coord *= 2. * texel_size / float(dim);
  vec3 sample_position = brick_start_coord + brick_offset + brick_coord;

  vec4 color = texture(voxel_bricks, sample_position);
  result.parent_ptr = result.node_ptr;
  result.node_ptr = voxel_idx;
  result.found = true;
  result.color = color;
  return result;
}

bool is_outside(ivec3 pos){
  return (pos.x > voxel_span || pos.y > voxel_span || pos.z > voxel_span || pos.x < 0 || pos.y < 0 || pos.z < 0);
}

float calculate_step(vec3 pos, vec3 r, vec3 r_inv, uint level){
  float cube_side =  cube_sides[level];
    //  float cube_side =  2 * 2048 / (voxel_span / pow(2,level));
  vec3 cube_coord = mod(pos, cube_side);
  float t = 1337;
  for(int i = 0; i < 3; i++){
    float cs = r[i] < 0 ? 0 : cube_side;
    float dt = (cs - cube_coord[i]) * r_inv[i];
    t = min(t, dt);
  }
  return t;
}


// Note that tracing can be done faster if child
SampleResult sample_step(vec3 pos, vec3 r, vec3 r_inv, out float t){
  SampleResult voxel = sample_octree(pos,DISPLAY_LEVEL);
  t = calculate_step(pos, r,r_inv, max(0,voxel.level-1));
  return voxel;
}

void main(){
  float fov_degrees = 60;
  float fov_radian = fov_degrees / 180 * 3.14169265;
  float fov_tan = tan(fov_radian / 2.0);

  vec3 span_x = cross(camera_dir, camera_up);

  vec2 uv = gl_FragCoord.xy / resolution;
  uv = uv * 2.0 - 1.0;
  uv *= fov_tan;
  vec3 plane_coord = span_x * uv.x + camera_up * uv.y;
  //plane_coord *= 50;
  plane_coord += camera_pos + camera_dir;

  vec3 r = normalize(plane_coord - camera_pos);
  vec3 r_inv = 1. / r;
  float t = 1.;

  vec3 cur_pos = camera_pos;
  float  level = 1.0;
  for(;;) {
    vec3 f_pos = (0.5 * (voxel_projection * vec4(cur_pos,1.)) + 0.5).xyz;
    ivec3 tex_coord = ivec3(voxel_span * f_pos);
    if(is_outside(tex_coord)){
      color = vec4(0.9,0.9,1,1.0);
      break;
    }
    //    vec3 texture_coord = cur_pos / ( world_box * 2) + 0.5;
    SampleResult voxel = sample_step(cur_pos,r,r_inv,t);// sample_step(cur_pos, r, 4, t);
    if(voxel.found){
      color = vec4(voxel.color.rgb,1.0);
      break;
    }
    cur_pos += (t + 0.1) * r;
  }
}
