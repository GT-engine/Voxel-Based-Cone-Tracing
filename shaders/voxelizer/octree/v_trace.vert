#version 460 core
layout (location = 0) in vec3 vertex;

out vec3 position;
void main(){
  position = vertex;
  gl_Position = vec4(vertex,1.0);
}
