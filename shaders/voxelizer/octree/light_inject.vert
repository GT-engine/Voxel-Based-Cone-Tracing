#version 450 core

layout (location = 0) in vec3 vertex;

out vec2 vert_pos;

void main(){
  vert_pos = vertex.xy;
  gl_Position = vec4(vertex, 1.0);
}
